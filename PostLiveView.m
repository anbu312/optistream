//
//  PostLiveView.m
//  SBN
//
//  Created by Sathish on 19/05/17.
//  Copyright © 2017 Optisol Business Solution. All rights reserved.
//

#import "PostLiveView.h"

@implementation PostLiveView
@synthesize doneBtn;
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (IBAction)doneButton:(id)sender{
    [self.delegate didDoneButtonTap];
}

@end
