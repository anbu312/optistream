//
//  User.h
//  InstanceChat
//
//  Created by Sathish on 31/05/17.
//  Copyright © 2017 Optisol Business Solutions pvt ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface LiveUser : NSObject

@property (nonatomic, strong) NSString *userName;
@property (nonatomic, strong) UIImage *userImg;

@end
