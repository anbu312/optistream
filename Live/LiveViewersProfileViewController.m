//
//  LiveViewersProfileViewController.m
//  InstanceChat
//
//  Created by Sathish on 30/05/17.
//  Copyright © 2017 Optisol Business Solutions pvt ltd. All rights reserved.
//

#import "LiveViewersProfileViewController.h"
#import "LiveUser.h"

@interface LiveViewersProfileViewController()<UICollectionViewDelegate,UICollectionViewDataSource>{
    NSMutableArray<LiveUser *> *users;
}
@end

@implementation LiveViewersProfileViewController

static NSString const *cellIdentifier = @"profileCellIdentifier";

-(void)viewDidLoad{
    [super viewDidLoad];
    users = [NSMutableArray array];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [users count];
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"profileCellIdentifier" forIndexPath:indexPath];
    cell.backgroundColor = [[UIColor grayColor] colorWithAlphaComponent:0.4];
    UIImageView *recipeImageView = (UIImageView *)[cell viewWithTag:100];
    LiveUser *user = [users objectAtIndex:indexPath.row];
    [recipeImageView setImage:user.userImg];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
}

-(void)refereshUserList{
    LiveUser *user = [[LiveUser alloc]init];
    user.userName = @"Sathish";
    [user setUserImg:[UIImage imageNamed:@"1.jpg"]];
    
    LiveUser *user2 = [[LiveUser alloc]init];
    user2.userName = @"Kumar";
    [user2 setUserImg:[UIImage imageNamed:@"22.jpg"]];
    
    [users addObject:user];
    [users addObject:user2];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [_userCollectionView reloadData];
    });
}
@end
