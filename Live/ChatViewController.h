//
//  ChatListViewController.h
//  InstanceChat
//
//  Created by Sathish on 27/05/17.
//  Copyright © 2017 Optisol Business Solutions pvt ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SLKTextViewController.h"

@protocol ChatViewControllerDelegate <NSObject>

@optional
-(void)enableUserChat;

@end
@interface ChatViewController :SLKTextViewController

@property (assign) id<ChatViewControllerDelegate> chatViewDelegate;

// Paging properties
@property NSUInteger pageIndex;
@property NSString *pageTitle;

- (void)setFrame:(UIView *)view;
- (void)hideKeyBoardWithInputBar;
- (void)showKeyBoardWithInputBar;
- (void)hideTextInputbar:(BOOL)flag;
- (void)enableUserChat;
- (void)connectWithTwilioClient:(NSString *)identity token:(NSString *)tempToken channel:(NSString *)tempChannelValue;

@end
