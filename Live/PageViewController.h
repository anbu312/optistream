//
//  PageViewController.h
//  InstanceChat
//
//  Created by Sathish on 27/05/17.
//  Copyright © 2017 Optisol Business Solutions pvt ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PageViewController : UIPageViewController
@property NSUInteger pageIndex;
@property NSString *imgFile;
@property NSString *txtTitle;
@end
