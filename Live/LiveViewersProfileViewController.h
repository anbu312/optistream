//
//  LiveViewersProfileViewController.h
//  InstanceChat
//
//  Created by Sathish on 30/05/17.
//  Copyright © 2017 Optisol Business Solutions pvt ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LiveViewersProfileViewController : UIViewController

// Paging properties
@property NSUInteger pageIndex;
@property NSString *pageTitle;

@property(nonatomic, strong) IBOutlet UICollectionView *userCollectionView;

- (void)refereshUserList;

@end
