//
//  ChatViewController.m
//  InstanceChat
//
//  Created by Sathish on 27/05/17.
//  Copyright © 2017 Optisol Business Solutions pvt ltd. All rights reserved.
//

#import "ChatViewController.h"
#import "MessageTableViewCell.h"
#import "MessageTextView.h"
#import "TypingIndicatorView.h"
#import <TwilioChatClient/TwilioChatClient.h>
#import "SDImageCache.h"
#import "UIImageView+WebCache.h"
#import "Constants.h"

#define DEBUG_CUSTOM_TYPING_INDICATOR 0

@interface ChatViewController()<TwilioChatClientDelegate>

@property (nonatomic, strong) NSMutableOrderedSet *messages;

@property (nonatomic, strong) NSString *identity;

@property (strong, nonatomic) TCHChannel *channel;
@property (strong, nonatomic) TwilioChatClient *client;

@property (nonatomic, strong) NSString *tempChannel;

@end

@implementation ChatViewController

- (instancetype)init
{
    self = [super initWithTableViewStyle:UITableViewStylePlain];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self commonInit];
    }
    return self;
}

+ (UITableViewStyle)tableViewStyleForCoder:(NSCoder *)decoder
{
    return UITableViewStylePlain;
}

- (void)commonInit
{
    [[NSNotificationCenter defaultCenter] addObserver:self.tableView selector:@selector(reloadData) name:UIContentSizeCategoryDidChangeNotification object:nil];
    
    self.messages = [[NSMutableOrderedSet alloc] init];
    
    // Register a SLKTextView subclass, if you need any special appearance and/or behavior customisation.
    [self registerClassForTextView:[MessageTextView class]];
    
#if DEBUG_CUSTOM_TYPING_INDICATOR
    // Register a UIView subclass, conforming to SLKTypingIndicatorProtocol, to use a custom typing indicator view.
    [self registerClassForTypingIndicatorView:[TypingIndicatorView class]];
#endif
}

- (void)configureComponents{
    // SLKTVC's configuration
    self.bounces = YES;
    self.shakeToClearEnabled = YES;
    self.keyboardPanningEnabled = YES;
    self.shouldScrollToBottomAfterKeyboardShows = NO;
    self.inverted = YES;
    
    [self.leftButton setImage:[UIImage imageNamed:@"icn_upload"] forState:UIControlStateNormal];
    [self.leftButton setTintColor:[UIColor grayColor]];
    
    [self.rightButton setTitle:NSLocalizedString(@"Send", nil) forState:UIControlStateNormal];
    
    self.textInputbar.autoHideRightButton = YES;
    self.textInputbar.maxCharCount = 500;
    self.textInputbar.counterStyle = SLKCounterStyleSplit;
    self.textInputbar.counterPosition = SLKCounterPositionTop;
    [self.textInputbar.editorTitle setTextColor:[UIColor darkGrayColor]];
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView registerClass:[MessageTableViewCell class] forCellReuseIdentifier:MessengerCellIdentifier];
    
    self.textView.keyboardType = UIKeyboardAppearanceDefault;
    self.textView.returnKeyType = UIReturnKeyDone;
    self.textView.enablesReturnKeyAutomatically = NO;
    self.textView.autocorrectionType = UITextAutocorrectionTypeYes;
    
#if !DEBUG_CUSTOM_TYPING_INDICATOR
    self.typingIndicatorView.canResignByTouch = YES;
#endif
    
    self.textInputbarHidden = YES;
}

#pragma mark - View lifecycle

- (void)viewDidLoad{
    [super viewDidLoad];
    [self configureComponents];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.view.backgroundColor = [UIColor clearColor];
    self.tableView.backgroundColor = [UIColor clearColor];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

#pragma mark - Action Methods

- (void)hideTextInputbar:(BOOL)isToHide{
    [self setTextInputbarHidden:isToHide animated:NO];
}

#pragma mark - Overriden Methods

- (void)didChangeKeyboardStatus:(SLKKeyboardStatus)status
{
    // Notifies the view controller that the keyboard changed status.
    
    switch (status) {
        case SLKKeyboardStatusWillShow:
            return NSLog(@"Will Show");
        case SLKKeyboardStatusDidShow:
            return NSLog(@"Did Show");
        case SLKKeyboardStatusWillHide:
            return NSLog(@"Will hide");
        case SLKKeyboardStatusDidHide:
            return NSLog(@"Did Hide");
    }
}

- (void)textWillUpdate
{
    // Notifies the view controller that the text will update.
    
    [super textWillUpdate];
}

- (void)textDidUpdate:(BOOL)animated
{
    // Notifies the view controller that the text did update.
    
    [super textDidUpdate:animated];
}

- (void)didPressLeftButton:(id)sender
{
    // Notifies the view controller when the left button's action has been triggered, manually.
    
    [super didPressLeftButton:sender];
    
    UIViewController *vc = [UIViewController new];
    vc.view.backgroundColor = [UIColor whiteColor];
    vc.title = @"Details";
    
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)didPressRightButton:(id)sender
{
    // Notifies the view controller when the right button's action has been triggered, manually or by using the keyboard return key.
    
    // This little trick validates any pending auto-correction or auto-spelling just after hitting the 'Send' button
    [self.textView refreshFirstResponder];
    [self hideKeyBoardWithInputBar];
    
    TCHMessage *message = [self.channel.messages createMessageWithBody:self.textView.text];
    
    [self.channel.messages sendMessage:message completion:^(TCHResult *result) {
        if (!result.isSuccessful) {
            NSLog(@"message not sent...");
        }else{
            [self updateChatList:sender message:message];
        }
    }];
    
    [super didPressRightButton:sender];
}

- (void)updateChatList:(id)sender message:(TCHMessage *)message{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    UITableViewRowAnimation rowAnimation = self.inverted ? UITableViewRowAnimationBottom : UITableViewRowAnimationTop;
    UITableViewScrollPosition scrollPosition = self.inverted ? UITableViewScrollPositionBottom : UITableViewScrollPositionTop;
    
    [self.tableView beginUpdates];
    [self.messages insertObject:message atIndex:0];
    [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:rowAnimation];
    [self.tableView endUpdates];
    
    [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:scrollPosition animated:YES];
    
    // Fixes the cell from blinking (because of the transform, when using translucent cells)
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
}

- (NSString *)keyForTextCaching
{
    return [[NSBundle mainBundle] bundleIdentifier];
}

- (void)willRequestUndo
{
    // Notifies the view controller when a user did shake the device to undo the typed text
    
    [super willRequestUndo];
}

- (void)didCancelTextEditing:(id)sender
{
    // Notifies the view controller when tapped on the left "Cancel" button
    
    [super didCancelTextEditing:sender];
}

- (BOOL)canPressRightButton
{
    return [super canPressRightButton];
}

- (BOOL)canShowTypingIndicator
{
#if DEBUG_CUSTOM_TYPING_INDICATOR
    return YES;
#else
    return [super canShowTypingIndicator];
#endif
}

#pragma mark - UITextViewDelegate Methods

- (BOOL)textView:(SLKTextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    NSString *txtLength = [textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if([text isEqualToString:@"\n"]){
        txtLength.length == 0?[self hideKeyBoardWithInputBar]   :[self didPressRightButton:nil];
        return false;
    }
    return [super textView:textView shouldChangeTextInRange:range replacementText:text];
}

// Hide keyboard autocorrection toolbar
-(BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    textView.autocorrectionType = UITextAutocorrectionTypeNo;
    return [super textViewShouldBeginEditing:textView];
}

#pragma mark - UITableViewDataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.messages.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self messageCellForRowAtIndexPath:indexPath];
}
- (MessageTableViewCell *)messageCellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MessageTableViewCell *cell = (MessageTableViewCell *)[self.tableView dequeueReusableCellWithIdentifier:MessengerCellIdentifier];
    
    TCHMessage *message = [self.messages objectAtIndex:indexPath.row];
    
    NSArray *tempAuthor = [message.author componentsSeparatedByString:@","];
    NSString *nameSplit;
    NSString *idSplit;
    NSString *imageSplit;
    if([tempAuthor count] == 2){
        nameSplit = [tempAuthor objectAtIndex:0];
        idSplit = [tempAuthor objectAtIndex:1];
    }else{
        nameSplit = [tempAuthor objectAtIndex:0];
        idSplit = [tempAuthor objectAtIndex:1];
        imageSplit = [tempAuthor objectAtIndex:2];
    }
    NSString *userImage = [NSString stringWithFormat:@"%@/%@/%@",CLOUD_FRONT,idSplit,imageSplit];
    [cell.thumbnailView sd_setImageWithURL:[NSURL URLWithString:userImage] placeholderImage:[UIImage imageNamed:@"defaultbanner.png"] options:SDWebImageRetryFailed completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
        if(image){
            cell.thumbnailView.image = image;
        }
    }];

    cell.titleLabel.text = nameSplit;
    cell.bodyLabel.text = message.body;
    
    cell.indexPath = indexPath;
    cell.usedForMessage = YES;
    
    // Cells must inherit the table view's transform
    // This is very important, since the main table view may be inverted
    cell.transform = self.tableView.transform;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView isEqual:self.tableView]) {
        TCHMessage *message = self.messages[indexPath.row];
        
        NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
        paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
        paragraphStyle.alignment = NSTextAlignmentLeft;
        
        CGFloat pointSize = [MessageTableViewCell defaultFontSize];
        
        NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:pointSize],NSParagraphStyleAttributeName: paragraphStyle};
        
        CGFloat width = CGRectGetWidth(tableView.frame)-kMessageTableViewCellAvatarHeight;
        width -= 25.0;
        
        NSArray *tempAuthor = [message.author componentsSeparatedByString:@","];
        NSString *nameSplit = [tempAuthor objectAtIndex:0];
        CGRect titleBounds = [nameSplit boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:NULL];
        CGRect bodyBounds = [message.body boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:NULL];
        
        if (message.body.length == 0) {
            return 0.0;
        }
        
        CGFloat height = CGRectGetHeight(titleBounds);
        height += CGRectGetHeight(bodyBounds);
        height += 15.0;
        
        if (height < kMessageTableViewCellMinimumHeight) {
            height = kMessageTableViewCellMinimumHeight;
        }
        
        return height;
    }
    else {
        return kMessageTableViewCellMinimumHeight;
    }
}

- (void)setFrame:(UIView *)view{
        self.view.frame = CGRectMake(0, 0, view.frame.size.width, view.frame.size.height);
}

#pragma mark - UITableViewDelegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

}

#pragma mark - UIScrollViewDelegate Methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    // Since SLKTextViewController uses UIScrollViewDelegate to update a few things, it is important that if you override this method, to call super.
    [super scrollViewDidScroll:scrollView];
}

- (void)hideKeyBoardWithInputBar{
    [self hideTextInputbar:true];
    [self.textView resignFirstResponder];
}

- (void)showKeyBoardWithInputBar{
    [self hideTextInputbar:false];
    [self.textView becomeFirstResponder];
}

- (void)enableUserChat{
    [self.chatViewDelegate enableUserChat];
}

#pragma mark - Twillio Configuration

- (void)connectWithTwilioClient:(NSString *)identity token:(NSString *)tempToken channel:(NSString *)tempChannelValue{
    
    // Set UserName as an identity
    self.identity = identity;
    self.tempChannel = tempChannelValue;
    
    [TwilioChatClient chatClientWithToken:tempToken properties:nil delegate:self completion:^(TCHResult *result, TwilioChatClient *chatClient) {
        if(result.error == nil){
            self.client = chatClient;
            self.client.delegate = self;
        }else{
            [self connectWithTwilioClient:identity token:tempToken channel:tempChannelValue];
        }
    }];
}

#pragma mark - TwilioChatClientDelegate

- (void)chatClient:(TwilioChatClient *)client synchronizationStatusUpdated:(TCHClientSynchronizationStatus)status{
    if (status == TCHClientSynchronizationStatusCompleted) {
        NSString *defaultChannel = self.tempChannel;
        
        [client.channelsList channelWithSidOrUniqueName:defaultChannel completion:^(TCHResult *result, TCHChannel *channel) {
            if (channel) {
                self.channel = channel;
                [self.channel joinWithCompletion:^(TCHResult *result) {
                    NSLog(@"joined vzggjrkm channel");
                    [self enableUserChat];
                }];
            } else {
                // Create the general channel (for public use) if it hasn't been created yet
                [client.channelsList createChannelWithOptions:@{
                                                                TCHChannelOptionFriendlyName: @"General Chat Channel",
                                                                TCHChannelOptionType: @(TCHChannelTypePublic)
                                                                }
                                                   completion:^(TCHResult *result, TCHChannel *channel) {
                                                       self.channel = channel;
                                                       [self.channel joinWithCompletion:^(TCHResult *result) {
                                                           [self.channel setUniqueName:defaultChannel completion:^(TCHResult *result) {
                                                               [self enableUserChat];
                                                               NSLog(@"channel unique name set");
                                                           }];
                                                       }];
                                                   }];
            }
        }];
    }
}

// load twilio cloud messages
- (void)addMessages:(NSArray<TCHMessage *> *)messages {
    dispatch_async(dispatch_get_main_queue(), ^{
        TCHMessage *msg =[messages objectAtIndex:[messages count]-1];
        NSArray *tempAuthor = [msg.author componentsSeparatedByString:@","];
        NSString *idSplit = [tempAuthor objectAtIndex:1];
        if(![idSplit isEqualToString:self.identity]){
            [self.messages insertObject:msg atIndex:0];
            [self.tableView reloadData];
        }
    });
}

- (void)chatClient:(TwilioChatClient *)client channel:(TCHChannel *)channel messageAdded:(TCHMessage *)message {
    [self addMessages:@[message]];
}


#pragma mark - Lifeterm
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
