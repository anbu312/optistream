//
//  EnlargePicViewController.h
//  Hype
//
//  Created by Sathish on 22/03/16.
//  Copyright © 2016 Optisol Business Solution 
//

#import "BaseViewController.h"
#import <AWSS3/AWSS3.h>
#import <AWSCore/AWSCore.h>
#import <AVKit/AVKit.h>
#import <AVFoundation/AVPlayer.h>
#import <AVFoundation/AVPlayerLayer.h>
#import <AVFoundation/AVAsset.h>
#import <AVFoundation/AVPlayerItem.h>
#import "Constants.h"
#import "AppDelegate.h"
#import "FeedPost.h"
#import <SafariServices/SafariServices.h>

@interface EnlargeRecordViewController : BaseViewController<UIGestureRecognizerDelegate,SFSafariViewControllerDelegate,AlertServiceDelegate,AVPlayerViewControllerDelegate>{
    FeedPost *feedPostList;
    NSString *feedId;
}
@property(strong, retain)UINavigationController *mainViewController;

@property (nonatomic, strong) IBOutlet UIView *carousel;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIButton *reportIcon;
@property (weak, nonatomic) IBOutlet UIButton *likeButton;
@property (weak, nonatomic) IBOutlet UILabel *countNumber;
@property (weak, nonatomic) IBOutlet UIButton *shareButton;
@property (weak, nonatomic) IBOutlet UIButton *commentButton;
@property (weak, nonatomic) IBOutlet UIButton *playBtn;
@property (weak, nonatomic) IBOutlet UIButton *viewsLabel;
@property (weak, nonatomic) IBOutlet UIButton *commentsLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) NSIndexPath *currentIndexPath;
@property (nonatomic) BOOL isEventOwner;
@property (nonatomic) BOOL isVideoContent;
@property (nonatomic,strong) NSString *currentItem;
@property (weak, nonatomic) NSDictionary *imageDic;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (nonatomic,strong) NSString *currentPostId;
@property (weak, nonatomic) IBOutlet UIView *scrollContentView;
@property (weak, nonatomic) IBOutlet UIView *scrollImageView;
@property (nonatomic,strong) NSString *liveUrl;
@property (nonatomic,strong) NSString *liveID;
@property (nonatomic,strong) NSString *isLiveFlag;
@property (nonatomic,strong) NSString *liveTitle;

@property (weak, nonatomic) NSURLSession *backgroundSession;
@property (weak, nonatomic) AWSS3 *S3;

@property (weak, nonatomic) NSString *imageName;
@property (strong, nonatomic) NSString *pageFlag;

@end
