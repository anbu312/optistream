//
//  VideoPlayerViewController.h
//  SDKSampleApp
//
//  This code and all components (c) Copyright 2015-2016, Wowza Media Systems, LLC. All rights reserved.
//  This code is licensed pursuant to the BSD 3-Clause License.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
@class WZStatus;

extern NSString *const SDKSampleSavedConfigKey;
extern NSString *const SDKSampleAppLicenseKey;

@interface VideoPlayerViewController : BaseViewController<AlertServiceDelegate>

@property(strong, retain)UINavigationController *mainViewController;

@property (strong, nonatomic) NSString *liveApplicationName;
@property (strong, nonatomic) NSString *livePortNumber;
@property (strong, nonatomic) NSString *liveHostAddress;
@property (strong, nonatomic) NSString *liveStreamName;
@property (strong, nonatomic) NSString *streamFlag;
@property (strong, nonatomic) NSString *liveStreamId;

@property (strong, nonatomic) NSString *liveUserName;
@property (strong, nonatomic) NSString *livePassword;

@property (weak, nonatomic) IBOutlet UILabel *connectLbl;
@property (strong, nonatomic) IBOutlet UIImageView *liveImage;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UIButton *chatBtn;

+ (void) showAlertWithTitle:(NSString *)title status:(WZStatus *)status presenter:(UIViewController *)presenter;
+ (void) showAlertWithTitle:(NSString *)title error:(NSError *)error presenter:(UIViewController *)presenter;


@end

