//
//  EnlargePicViewController.h
//  Hype
//
//  Created by Sathish on 22/03/16.
//  Copyright © 2016 Optisol Business Solution 
//

#import "BaseViewController.h"
#import "iCarousel.h"
#import <AWSS3/AWSS3.h>
#import <AWSCore/AWSCore.h>
#import <AVKit/AVKit.h>
#import <AVFoundation/AVPlayer.h>
#import <AVFoundation/AVPlayerLayer.h>
#import <AVFoundation/AVAsset.h>
#import <AVFoundation/AVPlayerItem.h>
#import "Constants.h"
#import "AppDelegate.h"
#import "FeedPost.h"
#import <SafariServices/SafariServices.h>
#import <JWPlayer_iOS_SDK/JWPlayerController.h>

@interface EnlargeLiveViewController : BaseViewController<UIGestureRecognizerDelegate,SFSafariViewControllerDelegate,AlertServiceDelegate>
    
@property(strong, retain)UINavigationController *mainViewController;
@property (nonatomic,strong) NSString *currentPostId;
@property (nonatomic,strong) NSString *liveUrl;
@property (nonatomic,strong) NSString *liveID;
@property (nonatomic,strong) NSString *isLiveFlag;
@property (nonatomic,strong) NSString *liveTitle;
@property (nonatomic, strong) IBOutlet UIView *playerContainer;

@property (weak, nonatomic) NSURLSession *backgroundSession;
@property (weak, nonatomic) AWSS3 *S3;

@property (weak, nonatomic) NSString *imageName;
@property (strong, nonatomic) NSString *pageFlag;

@property (weak, nonatomic) IBOutlet UIButton *chatBtn;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UIImageView *liveBtn;

@end
