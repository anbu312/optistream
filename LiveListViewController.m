//
//  FeedViewController.m
//  StarsBe
//
//  Created by Anbumani on 11/9/16.
//  Copyright © 2016 Optisol Business Solution Pvt Ltd
 
//

#import "LiveListViewController.h"
#import "CelebrityPostList.h"
#import "SDImageCache.h"
#import "UIImageView+WebCache.h"
#import "LikeManager.h"
#import "EnlargeLiveViewController.h"
#import "NIDropDown.h"
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <Social/Social.h>
#import <SafariServices/SafariServices.h>
#import "Post.h"
#import "DetailedPostViewController.h"
#import "UIView+UpdateAutoLayoutConstraints.h"
#import "OtherProfileViewController.h"
#import "HomeViewController.h"
#import "JTSImageViewController.h"
#import "JTSImageInfo.h"
#import "CustomYesNoAlert.h"
#import "KLCPopup.h"
#import "NavigationController.h"
#import "AppDelegate.h"
#import "CreateLivePostViewController.h"
#import "VideoPlayerViewController.h"
#import "EnlargeRecordViewController.h"
#import "TelentSearchViewController.h"
#import "FeedViewController.h"
#import "SubscriptionViewController.h"
#import "HomeViewController.h"
#import "CreatePostViewController.h"

@import Firebase;
@import FirebaseInstanceID;
@import FirebaseMessaging;

#ifndef NSFoundationVersionNumber_iOS_9_x_Max
#define NSFoundationVersionNumber_iOS_9_x_Max 1299
#endif

@interface LiveListViewController ()<NIDropDownDelegate,SFSafariViewControllerDelegate,UIApplicationDelegate>{
    NIDropDown *dropDown;
    NSArray *arr;
    NSArray *barArray;
    CGFloat height;
    NSMutableArray *feedArray;
    BOOL isFlag;
    int selectedIndex;
    NSDictionary *tempPostId;
    NSString * shareTitle, *shareDesc, *shareImage, *postId, *selectedStringValue, *isImageFlag, *tempThumbnailImage;
    NSString *countindex;
    int reloadIndexVal;
    int pagenationindex;
    int pageValue;
    int pageOffsetSize;
    CelebrityPostList* celebrityPostTempList;
    UIButton *menuButtonFlag;
    NSMutableArray *tempUserDict;
    BOOL enableCellHeight;
    NSMutableArray *tempPostArray;
    NSDictionary * enlargeDict;
    BOOL footerHeight;
    UIActivityIndicatorView *progressLoader;
    NSDictionary *tempProfileDict;
    UIRefreshControl *refreshControl;
}
@property Post * post;
@property KLCPopup* popup;

@end

@implementation LiveListViewController
-(void)viewWillDisappear:(BOOL)animated{
    pageValue = 0;
    //pagenationindex = 20;
}
-(void)PageConfig{
    self.serviceDelegate = [ServiceDelegate getInstance];
    self.appUtil.alrDelegate = self;
    self.serviceDelegate.delegate = self;
    [self.serviceDelegate resetRKObjectManager];
    [BaseViewController setHeader];
    NSError* err = nil;
    self.user = [[User alloc] initWithString:[Lib reteriveData:Access_Token] error:&err];
}
-(void)singleAlertBtnClicked{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [refreshControl endRefreshing];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"NOTIFICATION_POSTID"];
    [_loaderView setHidden:NO];
    self.livePostTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    enlargeDict = [[NSDictionary alloc] init];
    tempUserDict = [[NSMutableArray alloc] init];
    tempPostArray = [[NSMutableArray alloc] init];
    UINib *cellNib = [UINib nibWithNibName:@"LivePostLandScapeCell" bundle:nil];
    [self.livePostTableView registerNib:cellNib forCellReuseIdentifier:@"LivePostLandScapeCell"];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(selectRowAction)
                                                 name:@"FEED_PROFILE_VIEW"
                                               object:nil];

    countindex = @"50";
    pagenationindex = 50;
    reloadIndexVal = 50;
    pageOffsetSize = 50;
    pageValue = 0;
    dropDown=[[NIDropDown alloc]init];
    dropDown.delegate = self;
    height=150;
    [self PageConfig];
    self.post = [[Post alloc] init];
    self.user = [[User alloc] initWithString:[Lib reteriveData:Access_Token] error:nil];
    
    refreshControl = [[UIRefreshControl alloc]init];
    refreshControl.backgroundColor = [UIColor clearColor];
    refreshControl.tintColor = [UIColor blackColor];
    [refreshControl addTarget:self
                       action:@selector(refreshFeedView:)
             forControlEvents:UIControlEventValueChanged];
    [self.livePostTableView addSubview:refreshControl];
    [self loadFeedData:@"0"];
    [self addQuickNavigationTool];
}
-(void)addQuickNavigationTool{
    CGRect floatFrame = CGRectMake(0, 0, 60, 60);
    if (UIScreen.mainScreen.nativeScale==2.0) {
        floatFrame = CGRectMake([[UIScreen mainScreen] bounds].size.width-85, [[UIScreen mainScreen] bounds].size.height-200, 60, 60);
    }else{
        floatFrame = CGRectMake([[UIScreen mainScreen] bounds].size.width-82, [[UIScreen mainScreen] bounds].size.height-182, 60, 60);
    }
    self.addButton = [[VCFloatingActionButton alloc]initWithFrame:floatFrame normalImage:[UIImage imageNamed:@"add-post"] andPressedImage:[UIImage imageNamed:@"add-post"] withScrollview:self.livePostTableView];
    NSString *subStatus = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"SUB_STATUS"]];
    if([self.user.access_token.userInfo.userType isEqualToString:@"3"]){
        if([subStatus isEqualToString:@"active"]){
            self.addButton.labelArray = @[@"Create Post",@"Talent Search",@"VIP Feed",@"Feed",@"Profile"];
            self.addButton.imageArray = @[@"post-circle",@"trophy-circle",@"vip-grey-circle",@"feed-circle",@"profile-circle"];
        }else{
            self.addButton.labelArray = @[@"Create Post",@"Talent Search",@"VIP Feed",@"Feed",@"Profile"];
            self.addButton.imageArray = @[@"post-circle",@"trophy-circle",@"vip-grey-circle",@"feed-circle",@"profile-circle"];
        }
    }else{
        self.addButton.labelArray = @[@"Create Live Event",@"Create Post",@"Talent Search",@"VIP Feed",@"Feed",@"Profile"];
        self.addButton.imageArray = @[@"liveFeed-circle",@"post-circle",@"trophy-circle",@"vip-grey-circle",@"feed-circle",@"profile-circle"];
    }
    self.addButton.hideWhileScrolling = NO;
    self.addButton.delegate = self;
    [self.view addSubview:self.addButton];
//    [self.livePostTableView bringSubviewToFront:self.addButton];
}

-(void)menuButton:(id)sender{
    NSError *err;
    menuButtonFlag = (UIButton *)sender;
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.livePostTableView];
    NSIndexPath *indexPath = [self.livePostTableView indexPathForRowAtPoint:buttonPosition];
    [[NSUserDefaults standardUserDefaults] setObject:@{@"row": @(indexPath.row),
                                                       @"section": @(indexPath.section)}
                                              forKey:@"SELECTED_INDEX"];
    NSDictionary *temp = [self.post.posts objectAtIndex:indexPath.row];
    celebrityPostTempList =  [[CelebrityPostList alloc] initWithDictionary:temp error:&err];
    postId = celebrityPostTempList.liveId;
    [self deleteAction];

}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex==0){
        [self deletionAction];
    }
}

-(void)deletionAction{
    ResponseHandler callback = ^(BOOL wasSuccessful, RKMappingResult *mappingResult,RKObjectRequestOperation *operation) {
        DLOG(@"callback reponse %@",operation.HTTPRequestOperation.responseString);
        if(operation.HTTPRequestOperation.response.statusCode == 200){
            [self simpleAlert:@"Deleted sucessfully." withDelegate:self.view];
            [self loadFeedData:@"0"];
        }
    };
    NSString *feedUrl = [NSString stringWithFormat:@"%@%@",DELETE_STREAM,[NSString stringWithFormat:@"%@",postId]];
    [self.serviceDelegate executeDeleteURL:feedUrl param:nil responseMapping:self.serviceDelegate.dummyMapping keyPath:nil callBack:callback onView:self.view];
    
}

-(void)deleteAction{
    UIAlertView *deleteAlert = [[UIAlertView alloc] initWithTitle:nil message: [NSString stringWithFormat: @"Are you sure you want to delete live event"] delegate: self cancelButtonTitle: @"YES"  otherButtonTitles:@"NO",nil];
    [deleteAlert show];
}
-(void)refreshFeedView:(UIRefreshControl *)refresh {
    pageValue = 0;
    refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"Refreshing data..."];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMM d, h:mm a"];
    NSString *lastUpdated = [NSString stringWithFormat:@"Last updated on %@",
                             [formatter stringFromDate:[NSDate date]]];
    refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:lastUpdated];
    if([Lib isHasConnection]){
        [self loadFeedData:@"0"];
    }else{
        [refreshControl endRefreshing];
    }
}
-(IBAction)createPostAction:(id)sender{
    CreateLivePostViewController * postObj =[[CreateLivePostViewController alloc] initWithNibName:@"CreateLivePostViewController" bundle:nil];
    postObj.mainViewController = self.mainViewController;
    [self showViewController:postObj sender:nil];
}
-(void)loadFeedData:(NSString *)offsetValue{
    [refreshControl endRefreshing];
    ResponseHandler callback = ^(BOOL wasSuccessful, RKMappingResult *mappingResult,RKObjectRequestOperation *operation) {
        DLOG(@"callback reponse %@",operation.HTTPRequestOperation.responseString);
        NSError *err;
        Post *p = nil;
        if(err == nil) {
            NSString* userString=operation.HTTPRequestOperation.responseString;
            if([offsetValue isEqualToString:@"0"]){
                self.post = [[Post alloc] initWithString:userString error:&err];
            }else{
                p = [[Post alloc] initWithString:userString error:&err];
                [tempPostArray addObjectsFromArray:p.posts];
                [p.posts removeAllObjects];
                p = nil;
            }
        }
        if([offsetValue isEqualToString:@"0"]){
            [self.livePostTableView reloadData];
        }
    };
    NSString *feedUrl;
    int tempOffsetVal;
    if([offsetValue isEqualToString:@"0"]){
        tempOffsetVal = 0;
    }else{
        int tempOffset = [offsetValue intValue];
        int tempPageSize =  tempOffset + 1;
        tempPageSize = tempPageSize * 50;
        pagenationindex = tempPageSize;
        
        int tempOffsetPageSize =  tempOffset;
        tempOffsetPageSize = tempOffsetPageSize * 50;
        tempOffsetVal  = tempOffsetPageSize;
    }
    
    feedUrl = [NSString stringWithFormat:@"%@?streamSize=%@&streamOffset=%d",GET_LIVE_STREAM,@"50",tempOffsetVal];
    if([offsetValue isEqualToString:@"0"]){
        [self.serviceDelegate executeGetURL:feedUrl param:nil responseMapping:self.serviceDelegate.dummyMapping keyPath:nil callBack:callback onView:self.view];
    }else{
        [self.serviceDelegate executeGetURLNotLoading:feedUrl param:nil responseMapping:self.serviceDelegate.dummyMapping keyPath:nil callBack:callback onView:self.view];
    }
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.user = [[User alloc] initWithString:[Lib reteriveData:Access_Token] error:nil];
    if([self.user.access_token.userInfo.userType isEqualToString:@"3"]){
        [_createLiveBtn setHidden:YES];
    }
    [_loaderView setHidden:YES];
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    UIBarButtonItem *barButtonItemLeft = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:string_img_menu] imageWithRenderingMode: UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(menuAction:)];
    self.navigationItem.leftBarButtonItem = barButtonItemLeft;
    self.navigationItem.title = @"Live Event";

    footerHeight  = NO;
    
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:246.0f/255.0f green:184.0f/255.0f blue:9.0f/255.0f alpha:1.0f];
    self.navigationController.navigationBar.tintColor=[UIColor whiteColor];
    self.navigationController.navigationBar.translucent=NO;
//    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
    self.navigationController.navigationBar.titleTextAttributes = @{
                                                                    NSFontAttributeName:[UIFont fontWithName:@"KaushanScript-Regular" size:18],
                                                                    NSForegroundColorAttributeName: [UIColor whiteColor]
                                                                    };
}
#pragma mark - Navigation bar Actions
- (void)menuAction:(id)sender{
    [self.frostedViewController presentMenuViewController];
}

-(void)pagenationMethod:(NSString *)pageSize{
    [self loadFeedData:pageSize];
}
- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath  *)indexPath{
    return YES;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.post.posts.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    [progressLoader stopAnimating];
    [_loaderView setHidden:YES];

    NSError *err;
    if(isFlag == YES){
        isFlag=NO;
        [dropDown hideDropDown:menuButtonFlag];
    }
    static NSString *simpleTableIdentifier = nil;
    simpleTableIdentifier = @"LivePostLandScapeCell";
    LivePostLandScapeCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil) {
        cell = [self.livePostTableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    };
    CelebrityPostList* celebrityPostlists =  [[CelebrityPostList alloc] initWithDictionary:[self.post.posts objectAtIndex:indexPath.row] error:&err];
    if(celebrityPostlists == nil){
        celebrityPostlists = [self.post.posts objectAtIndex:indexPath.row];
    }

    if (celebrityPostlists != nil) {
        cell.imgHeightConstant.constant = 300;
        NSArray *tempRatio = [celebrityPostlists.ratio componentsSeparatedByString:@":"];
        double tempHeight = [tempRatio[1] doubleValue];
        double feedImageHeight = cell.imgHeightConstant.constant * tempHeight;
        cell.imgHeightConstant.constant = feedImageHeight;
        //});
        
        [cell.starBtn setHidden:YES];
        NSString *tempSub = [NSString stringWithFormat:@"%@",celebrityPostlists.createdBy.isActiveSubscription];
        NSString *tempUserType = [NSString stringWithFormat:@"%@",celebrityPostlists.createdBy.userType];
        if([tempUserType isEqualToString:@"3"]){
            if([tempSub isEqualToString:@"1"]){
                [cell.starBtn setHidden:NO];
                [cell.starBtn setImage:[UIImage imageNamed:@"star-gold"]];
            }
        }else if ([tempUserType isEqualToString:@"4"]){
            [cell.starBtn setHidden:NO];
            [cell.starBtn setImage:[UIImage imageNamed:@"star-blue"]];
        }else{
            [cell.starBtn setHidden:NO];
            [cell.starBtn setImage:[UIImage imageNamed:@"star-red"]];
        }

        self.user = [[User alloc] initWithString:[Lib reteriveData:Access_Token] error:nil];
        [cell.menuDeleteBtn setHidden:YES];
        if([self.user.access_token.userInfo._id isEqual:celebrityPostlists.createdBy._id]){
            [cell.menuDeleteBtn setHidden:NO];
            cell.menuDeleteBtn.userInteractionEnabled = YES;
            [cell.menuDeleteBtn setImage:[UIImage imageNamed:@"delete-sidemenu-active"] forState:UIControlStateNormal];
        }else{
            [cell.menuDeleteBtn setHidden:YES];
            cell.menuDeleteBtn.userInteractionEnabled = NO;
        }
        cell.delegate = self;
        cell.subContainerView.layer.shadowOffset = CGSizeMake(0, 3);
        cell.subContainerView.layer.shadowOpacity = 0.5;

        NSString *dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
        NSString *outputDateFormat = @"MMM d',' yyyy";
        NSTimeZone *inputTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
        NSDateFormatter *inputDateFormatter = [[NSDateFormatter alloc] init];
        [inputDateFormatter setTimeZone:inputTimeZone];
        [inputDateFormatter setDateFormat:dateFormat];
        
        DLOG(@"%@",celebrityPostlists.publishedTime);
        DLOG(@"%@",celebrityPostlists.createdAt);
        NSDate *date = [inputDateFormatter dateFromString:celebrityPostlists.publishedTime];
        NSTimeZone *outputTimeZone = [NSTimeZone localTimeZone];
        NSDateFormatter *outputDateFormatter = [[NSDateFormatter alloc] init];
        [outputDateFormatter setTimeZone:outputTimeZone];
        [outputDateFormatter setDateFormat:outputDateFormat];
        
        NSInteger min = [[[NSCalendar currentCalendar] components: NSCalendarUnitMinute
                                                         fromDate: date
                                                           toDate: [NSDate date]
                                                          options: 0] minute];
        NSInteger hours = [[[NSCalendar currentCalendar] components: NSCalendarUnitHour
                                                           fromDate: date
                                                             toDate: [NSDate date]
                                                            options: 0] hour];
        
        NSInteger days = [[[NSCalendar currentCalendar] components: NSCalendarUnitDay
                                                          fromDate: date
                                                            toDate: [NSDate date]
                                                           options: 0] day];
        NSInteger months = [[[NSCalendar currentCalendar] components: NSCalendarUnitMonth
                                                            fromDate: date
                                                              toDate: [NSDate date]
                                                             options: 0] month];
        
        if (min < 59) {
            if(min == -1){
                cell.lblTime.text = [NSString stringWithFormat:@"Just now"];
            }else{
                cell.lblTime.text = [NSString stringWithFormat:@"%ld m",(long)min];
            }
        }else if(min > 59){
            if(hours > 23){
                if (days > 30) {
                    cell.lblTime.text = [NSString stringWithFormat:@"%ld month",(long)months];
                }else{
                    cell.lblTime.text = [NSString stringWithFormat:@"%ld d",(long)days];
                }
                
            }else{
                cell.lblTime.text = [NSString stringWithFormat:@"%ld h",(long)hours];
            }
        }
        NSString *feedTitles;
        NSString * profileImage;
        
        if([celebrityPostlists.createdBy._id isEqualToString: self.user.access_token.userInfo._id] && [celebrityPostlists.wall._id isEqualToString: self.user.access_token.userInfo._id]){
            //Creater our own wall
            profileImage = [NSString stringWithFormat:@"%@",celebrityPostlists.createdBy.profileImage];

                feedTitles = [NSString stringWithFormat:@"%@ %@ Added %@ Event",celebrityPostlists.createdBy.firstname,celebrityPostlists.createdBy.lastname,celebrityPostlists.tag];
            }else if([celebrityPostlists.createdBy._id isEqualToString: self.user.access_token.userInfo._id] && ![celebrityPostlists.wall._id isEqualToString: self.user.access_token.userInfo._id]){
                //Creater different wall
                profileImage = [NSString stringWithFormat:@"%@",celebrityPostlists.createdBy.profileImage];

                feedTitles = [NSString stringWithFormat:@"%@ %@ Added %@ Event on %@'s wall",celebrityPostlists.createdBy.firstname,celebrityPostlists.createdBy.lastname,celebrityPostlists.tag,celebrityPostlists.wall.firstname];
            }else if(![celebrityPostlists.createdBy._id isEqualToString: self.user.access_token.userInfo._id] && [celebrityPostlists.wall._id isEqualToString: self.user.access_token.userInfo._id]){
                //Other User post create our own wall
                profileImage = [NSString stringWithFormat:@"%@",celebrityPostlists.createdBy.profileImage];

                feedTitles = [NSString stringWithFormat:@"%@ %@ Added %@ Event on your wall",celebrityPostlists.createdBy.firstname,celebrityPostlists.createdBy.lastname,celebrityPostlists.tag];
            }else if([celebrityPostlists.createdBy._id isEqualToString: self.user.access_token.userInfo._id] && ![celebrityPostlists.sharedInfo._id isEqualToString: self.user.access_token.userInfo._id]){
                //Share section
                profileImage = [NSString stringWithFormat:@"%@",celebrityPostlists.sharedInfo.profileImage];

                feedTitles = [NSString stringWithFormat:@"%@ %@ Shared %@ %@'s Event %@",celebrityPostlists.sharedInfo.firstname,celebrityPostlists.sharedInfo.lastname,celebrityPostlists.createdBy.firstname,celebrityPostlists.createdBy.lastname,celebrityPostlists.tag];
            }else if(![celebrityPostlists.createdBy._id isEqualToString:celebrityPostlists.wall._id]){
                profileImage = [NSString stringWithFormat:@"%@",celebrityPostlists.createdBy.profileImage];

                feedTitles = [NSString stringWithFormat:@"%@ %@ Added %@ Event on %@ %@'s wall",celebrityPostlists.createdBy.firstname,celebrityPostlists.createdBy.lastname,celebrityPostlists.tag,celebrityPostlists.wall.firstname,celebrityPostlists.wall.lastname];
            }else if(![celebrityPostlists.createdBy._id isEqualToString: self.user.access_token.userInfo._id] && [celebrityPostlists.sharedInfo._id isEqualToString: self.user.access_token.userInfo._id]){
                //Share Section
                profileImage = [NSString stringWithFormat:@"%@",celebrityPostlists.sharedInfo.profileImage];

                feedTitles = [NSString stringWithFormat:@"You Shared %@ %@'s Event %@",celebrityPostlists.createdBy.firstname,celebrityPostlists.createdBy.lastname,celebrityPostlists.tag];
            }else{
                profileImage = [NSString stringWithFormat:@"%@",celebrityPostlists.createdBy.profileImage];

                feedTitles = [NSString stringWithFormat:@"%@ %@ Added %@ Event",celebrityPostlists.createdBy.firstname,celebrityPostlists.createdBy.lastname,celebrityPostlists.tag];
            }
        if (![profileImage isEqual:[NSNull null]]) {
            [cell.userImg sd_setImageWithURL:[NSURL URLWithString:profileImage] placeholderImage:nil options:SDWebImageRetryFailed completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
                if(image){
                    cell.userImg.image = image;
                }
            }];
        }
        //Create mutable string from original one
        NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:feedTitles];
        
        NSRange celebNameShare = [feedTitles rangeOfString:@"shared"];
        [attString addAttribute:NSForegroundColorAttributeName value:[UIColor lightGrayColor] range:celebNameShare];
        NSRange tagNameShare = [feedTitles rangeOfString:celebrityPostlists.tag];
        [attString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:253.0/255.0 green:221.0/255.0 blue:105.0/255.0 alpha:1.0] range:tagNameShare];
        [attString addAttribute: NSFontAttributeName value:[UIFont fontWithName:@"OpenSans-Bold" size:12] range:tagNameShare];
        
        cell.feedTitle.attributedText = attString;
        cell.lblPostTitle.text = celebrityPostlists.name;
        if(celebrityPostlists.postDescription.length == 0){
            [cell.feedDescriptions hideByHeight:YES];
            [cell.feedDescriptions setConstraintConstant:0 forAttribute:NSLayoutAttributeHeight];
        }else{
            [cell.feedDescriptions hideByHeight:NO];
            cell.feedDescriptions.text = celebrityPostlists.postDescription;
        }
        [cell.playBtn setHidden:NO];
        
        [cell.feedImage sd_setImageWithURL:[NSURL URLWithString:celebrityPostlists.file.largeThumbnailUrl] placeholderImage:nil options:SDWebImageRetryFailed completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
            if(image){
                cell.feedImage.clipsToBounds = YES;
                cell.feedImage.contentMode = UIViewAutoresizingFlexibleBottomMargin;
                cell.feedImage.layer.masksToBounds = YES;
            }
        }];
        if([celebrityPostlists.isLive boolValue] == 1){
            cell.userImg.layer.borderWidth = 2.0;
            cell.userImg.layer.borderColor = [UIColor greenColor].CGColor;
        }else{
            cell.userImg.layer.borderWidth = 0.0;
            cell.userImg.layer.borderColor = [UIColor whiteColor].CGColor;
        }
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTaped:)];
        singleTap.numberOfTapsRequired = 1;
        singleTap.numberOfTouchesRequired = 1;
        [cell.feedImage addGestureRecognizer:singleTap];
        [cell.feedImage setUserInteractionEnabled:YES];
        
        UITapGestureRecognizer *playImageTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTaped:)];
        playImageTap.numberOfTapsRequired = 1;
        playImageTap.numberOfTouchesRequired = 1;
        [cell.playIconImage addGestureRecognizer:playImageTap];
        [cell.playIconImage setUserInteractionEnabled:YES];
        
        // Tag set for accessing delegate method
        cell.optionsView.tag = indexPath.row;
        cell.editBtn.tag = indexPath.row;
        cell.socialBtn.tag = indexPath.row;
        cell.deleteBtn.tag = indexPath.row;
        cell.optionsBtn.tag = indexPath.row;
        
    }
    if (indexPath.row == pagenationindex - 42) {
        NSLog(@"last data");
        pageValue ++;
        reloadIndexVal  = pagenationindex;
        NSString *pageCount = [NSString stringWithFormat:@"%d",pageValue];
        [self pagenationMethod:pageCount];
    }
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSError *err;
    CelebrityPostList* celebrityPostlists =  [[CelebrityPostList alloc] initWithDictionary:[self.post.posts objectAtIndex:indexPath.row] error:&err];
    if(celebrityPostlists == nil){
        celebrityPostlists = [self.post.posts objectAtIndex:indexPath.row];
    }
    NSArray *tempRatio = [celebrityPostlists.ratio componentsSeparatedByString:@":"];
    double tempHeight = [tempRatio[1] doubleValue];
    double feedImageHeight = 300 * tempHeight;
    double cellHeight;
    if(tempHeight < 0.75){
        cellHeight = 461 + feedImageHeight - 275;
    }else{
        cellHeight = 461 + feedImageHeight - 220;
    }
    return cellHeight;
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == reloadIndexVal - 1){
        [_loaderView setBackgroundColor:[UIColor whiteColor]];
        
        progressLoader = [[UIActivityIndicatorView alloc] initWithFrame: CGRectMake(self.livePostTableView.frame.origin.y, self.livePostTableView.frame.origin.y, self.livePostTableView.frame.size.width, 20.0)];
        progressLoader.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
        
        [_loaderView addSubview:progressLoader];
        [progressLoader startAnimating];
        
        [_loaderView setHidden:NO];

        footerHeight = YES;
        reloadIndexVal = pagenationindex;
        [self.post.posts addObjectsFromArray:tempPostArray];
        [tempPostArray removeAllObjects];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.livePostTableView reloadData];
        });
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(isFlag == YES){
        isFlag=NO;
        [dropDown hideDropDown:menuButtonFlag];
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return section = 0;
}

- (void)imageTaped:(UIGestureRecognizer *)gestureRecognizer {
    NSError * err;
    if(isFlag == YES){
        isFlag=NO;
        [dropDown hideDropDown:menuButtonFlag];
    }
    CGPoint buttonPosition = [gestureRecognizer.view convertPoint:CGPointZero toView:self.livePostTableView];
    NSIndexPath *indexPath = [self.livePostTableView indexPathForRowAtPoint:buttonPosition];
    NSDictionary *temp = [self.post.posts objectAtIndex:indexPath.row];
    CelebrityPostList* celebrityPostlists =  [[CelebrityPostList alloc] initWithDictionary:temp error:&err];
        [[NSUserDefaults standardUserDefaults] setObject:@{@"row": @(indexPath.row),
                                                           @"section": @(indexPath.section)}
                                                  forKey:@"SELECTED_INDEX"];
        NSString *tempPageValue = [NSString stringWithFormat:@"%d",pageValue];
        [[NSUserDefaults standardUserDefaults] setObject:tempPageValue forKey:@"CURRENT_PATH"];
    if ([celebrityPostlists.isStreamed boolValue] == 1) {
//        EnlargeLiveViewController * EPVC =[[EnlargeLiveViewController alloc] initWithNibName:@"EnlargeLiveViewController" bundle:nil];
//        EPVC.currentPostId = [NSString stringWithFormat:@"%@",celebrityPostlists._id];
//        EPVC.pageFlag = @"Feed";
//        EPVC.liveUrl = [NSString stringWithFormat:@"%@",celebrityPostlists.liveUrl];
//        EPVC.liveID = [NSString stringWithFormat:@"%@",celebrityPostlists.liveId];
//        EPVC.isLiveFlag = [NSString stringWithFormat:@"%@",celebrityPostlists.isLive];
//        EPVC.liveTitle =[NSString stringWithFormat:@"%@",celebrityPostlists.name];
//        
//        EPVC.mainViewController = self.mainViewController;
//        [self showViewController:EPVC sender:nil];

        //Recording video
        EnlargeRecordViewController * EPVC =[[EnlargeRecordViewController alloc] initWithNibName:@"EnlargeRecordViewController" bundle:nil];
        EPVC.currentPostId = [NSString stringWithFormat:@"%@",celebrityPostlists._id];
        EPVC.pageFlag = @"Feed";
        EPVC.liveUrl = [NSString stringWithFormat:@"%@",celebrityPostlists.liveUrl];
        EPVC.liveID = [NSString stringWithFormat:@"%@",celebrityPostlists.liveId];
        EPVC.isLiveFlag = [NSString stringWithFormat:@"%@",celebrityPostlists.isLive];
        EPVC.liveTitle =[NSString stringWithFormat:@"%@",celebrityPostlists.name];
        EPVC.mainViewController = self.mainViewController;
        [self showViewController:EPVC sender:nil];
    }else if([celebrityPostlists.createdBy._id isEqualToString:self.user.access_token.token.userId] && [celebrityPostlists.isLive boolValue] == 0 && [celebrityPostlists.isStreamed boolValue] == 0){
        //Go Live
                UIStoryboard* storyboard = [UIStoryboard storyboardWithName:string_main bundle:nil];
                VideoPlayerViewController * postObj =[storyboard instantiateViewControllerWithIdentifier:@"VideoPlayerViewController"];
                postObj.mainViewController = self.mainViewController;
                postObj.livePortNumber = [NSString stringWithFormat:@"%@",celebrityPostlists.livePort];
                postObj.liveStreamName = [NSString stringWithFormat:@"%@",celebrityPostlists.liveStreamName];
                postObj.liveHostAddress = [NSString stringWithFormat:@"%@",celebrityPostlists.liveHost];
                postObj.liveApplicationName = [NSString stringWithFormat:@"%@",celebrityPostlists.liveApplication];
                postObj.liveStreamId = [NSString stringWithFormat:@"%@",celebrityPostlists.liveId];
                postObj.streamFlag = @"Yes";
                [self showViewController:postObj sender:nil];
//            }else{
//                EnlargeLiveViewController * EPVC =[[EnlargeLiveViewController alloc] initWithNibName:@"EnlargeLiveViewController" bundle:nil];
//                EPVC.currentPostId = [NSString stringWithFormat:@"%@",celebrityPostlists._id];
//                EPVC.pageFlag = @"Feed";
//                EPVC.liveUrl = [NSString stringWithFormat:@"%@",celebrityPostlists.liveUrl];
//                EPVC.liveID = [NSString stringWithFormat:@"%@",celebrityPostlists.liveId];
//                EPVC.isLiveFlag = [NSString stringWithFormat:@"%@",celebrityPostlists.isLive];
//                EPVC.liveTitle =[NSString stringWithFormat:@"%@",celebrityPostlists.name];
//                EPVC.mainViewController = self.mainViewController;
//                [self showViewController:EPVC sender:nil];
//            }
    }else if([celebrityPostlists.isLive boolValue] == 1){
        //Live Video
                EnlargeLiveViewController * EPVC =[[EnlargeLiveViewController alloc] initWithNibName:@"EnlargeLiveViewController" bundle:nil];
                EPVC.currentPostId = [NSString stringWithFormat:@"%@",celebrityPostlists._id];
                EPVC.pageFlag = @"Feed";
                EPVC.liveUrl = [NSString stringWithFormat:@"%@",celebrityPostlists.liveUrl];
                EPVC.liveID = [NSString stringWithFormat:@"%@",celebrityPostlists.liveId];
                EPVC.isLiveFlag = [NSString stringWithFormat:@"%@",celebrityPostlists.isLive];
                EPVC.liveTitle =[NSString stringWithFormat:@"%@",celebrityPostlists.name];

                EPVC.mainViewController = self.mainViewController;
                [self showViewController:EPVC sender:nil];
        }
   // }
}
-(void)profileImageTapped:(UIButton *)sender{
    if(isFlag == YES){
        isFlag=NO;
        [dropDown hideDropDown:menuButtonFlag];
    }
    NSError *err;
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.livePostTableView];
    NSIndexPath *indexPath = [self.livePostTableView indexPathForRowAtPoint:buttonPosition];
    NSDictionary *temp = [self.post.posts objectAtIndex:indexPath.row];
    CelebrityPostList* celebrityPostlists =  [[CelebrityPostList alloc] initWithDictionary:temp error:&err];
        HomeViewController *homeObj = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
        homeObj.pageFlag = @"OtherProfile";
        homeObj.otherUserId = celebrityPostlists.createdBy._id;
        homeObj.otherUsername = celebrityPostlists.createdBy.username;
        [self showViewController:homeObj sender:nil];
}
-(void)selectRowAction{
    [_popup dismissPresentingPopup];
    NSString *FlagId = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"OTHER_ID"]];
   // if(![FlagId isEqualToString:self.user.access_token.token.userId]){
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:string_main bundle:nil];
        HomeViewController *homeObj = [storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
        homeObj.pageFlag = @"OtherProfile";
        homeObj.otherUserId = FlagId;
        [self showViewController:homeObj sender:nil];
}

-(void)didSelectMenuOptionAtIndex:(NSInteger)row{
    NSLog(@"Floating action tapped index %tu",row);
    NSString *subStatus = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"SUB_STATUS"]];
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    if([self.user.access_token.userInfo.userType isEqualToString:@"3"]){
        if([subStatus isEqualToString:@"active"]){
            if (row==0) {
                CreatePostViewController *postObj = [sb instantiateViewControllerWithIdentifier:@"CreatePostViewController"];
                postObj.mainViewController = self.mainViewController;
//                [_mainViewController pushViewController:postObj animated:YES];
                [self showViewController:postObj sender:nil];

            }else if(row == 1){
                TelentSearchViewController *logObj=[[TelentSearchViewController alloc] initWithNibName:@"TelentSearchViewController" bundle:nil];
                logObj.otherUserFlag = @"TelentSearch";
//                [_mainViewController pushViewController:logObj animated:YES];
                [self showViewController:logObj sender:nil];

            }else if(row == 2){
                if([self.user.access_token.userInfo.userType isEqualToString:@"3"] && ![subStatus isEqualToString:@"active"]){
                    SubscriptionViewController * subObj =[[SubscriptionViewController alloc] initWithNibName:@"SubscriptionViewController" bundle:nil];
                    subObj.mainViewController = self.navigationController;
                    [self showViewController:subObj sender:nil];
                }else{
                    FeedViewController *logObj = [sb instantiateViewControllerWithIdentifier:@"FeedViewController"];
                    logObj.mainViewController = self.navigationController;
                    logObj.liveFeedFlag = @"Yes";
//                    [_mainViewController pushViewController:logObj animated:YES];
                    [self showViewController:logObj sender:nil];

                }
            }else if (row==3){
                FeedViewController *homeObj = [sb instantiateViewControllerWithIdentifier:@"FeedViewController"];
                homeObj.mainViewController = self.navigationController;
//                [_mainViewController pushViewController:homeObj animated:NO];
                [self showViewController:homeObj sender:nil];

            }else{
            HomeViewController *logObj = [sb instantiateViewControllerWithIdentifier:@"HomeViewController"];
            logObj.pageFlag = @"Profile";
            [self showViewController:logObj sender:nil];

//            [_mainViewController pushViewController:logObj animated:YES];
        }
    }else{
        if (row==0) {
            CreatePostViewController *postObj = [sb instantiateViewControllerWithIdentifier:@"CreatePostViewController"];
            postObj.mainViewController = self.mainViewController;
            [_mainViewController pushViewController:postObj animated:YES];
            
        }else if(row == 1){
            TelentSearchViewController *logObj=[[TelentSearchViewController alloc] initWithNibName:@"TelentSearchViewController" bundle:nil];
            logObj.otherUserFlag = @"TelentSearch";
            [_mainViewController pushViewController:logObj animated:YES];
        }else if(row == 2){
            if([self.user.access_token.userInfo.userType isEqualToString:@"3"] && ![subStatus isEqualToString:@"active"]){
                SubscriptionViewController * subObj =[[SubscriptionViewController alloc] initWithNibName:@"SubscriptionViewController" bundle:nil];
                subObj.mainViewController = self.navigationController;
                [self showViewController:subObj sender:nil];
            }else{
                FeedViewController *logObj = [sb instantiateViewControllerWithIdentifier:@"FeedViewController"];
                logObj.mainViewController = self.navigationController;
                logObj.liveFeedFlag = @"Yes";
                [_mainViewController pushViewController:logObj animated:YES];
            }
        }else if (row==3){
            FeedViewController *homeObj = [sb instantiateViewControllerWithIdentifier:@"FeedViewController"];
            homeObj.mainViewController = self.navigationController;
            [self.navigationController pushViewController:homeObj animated:NO];
        }else{
            HomeViewController *logObj = [sb instantiateViewControllerWithIdentifier:@"HomeViewController"];
            logObj.pageFlag = @"Profile";
            [_mainViewController pushViewController:logObj animated:YES];
        }
    }
    }else{
        if (row==0) {
            CreateLivePostViewController * postObj =[[CreateLivePostViewController alloc] initWithNibName:@"CreateLivePostViewController" bundle:nil];
            postObj.mainViewController = self.mainViewController;
            [self showViewController:postObj sender:nil];
        }else if(row==1){
            CreatePostViewController *postObj = [sb instantiateViewControllerWithIdentifier:@"CreatePostViewController"];
            postObj.mainViewController = self.mainViewController;
            [_mainViewController pushViewController:postObj animated:YES];
        }
        else if(row == 2){
            TelentSearchViewController *logObj=[[TelentSearchViewController alloc] initWithNibName:@"TelentSearchViewController" bundle:nil];
            logObj.otherUserFlag = @"TelentSearch";
            [_mainViewController pushViewController:logObj animated:YES];
        }else if(row == 3){
            if([self.user.access_token.userInfo.userType isEqualToString:@"3"] && ![subStatus isEqualToString:@"active"]){
                SubscriptionViewController * subObj =[[SubscriptionViewController alloc] initWithNibName:@"SubscriptionViewController" bundle:nil];
                subObj.mainViewController = self.navigationController;
                [self showViewController:subObj sender:nil];
            }else{
                FeedViewController *logObj = [sb instantiateViewControllerWithIdentifier:@"FeedViewController"];
                logObj.mainViewController = self.navigationController;
                logObj.liveFeedFlag = @"Yes";
                [_mainViewController pushViewController:logObj animated:YES];
            }
        }else if (row==4){
            FeedViewController *homeObj = [sb instantiateViewControllerWithIdentifier:@"FeedViewController"];
            homeObj.mainViewController = self.navigationController;
            [self.navigationController pushViewController:homeObj animated:NO];
        }else{
            HomeViewController *logObj = [sb instantiateViewControllerWithIdentifier:@"HomeViewController"];
            logObj.pageFlag = @"Profile";
            [_mainViewController pushViewController:logObj animated:YES];
        }

    }
}
@end
