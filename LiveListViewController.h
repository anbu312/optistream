//
//  FeedViewController.h
//  StarsBe
//
//  Created by Anbumani on 11/9/16.
//  Copyright © 2016 Optisol Business Solution Pvt Ltd
 
//

#import "BaseViewController.h"
#import "FeedPostList.h"
#import "FeedPost.h"
#import "PostCell.h"
#import "LivePostLandScapeCell.h"
#import "UserListView.h"
#import "VCFloatingActionButton.h"
@interface LiveListViewController : BaseViewController<UIActionSheetDelegate, AlertServiceDelegate, UINavigationControllerDelegate, UITableViewDelegate , PostCellDelegate,UserTableCellDelegate,floatMenuDelegate>{
    FeedPost *feesPostList;
}
@property(strong, retain)UINavigationController *mainViewController;
@property (weak, nonatomic) IBOutlet UITableView *livePostTableView;
@property (weak, nonatomic) IBOutlet UILabel *noDataLbl;
@property (weak, nonatomic) IBOutlet UIButton *createLiveBtn;
@property (strong, nonatomic) VCFloatingActionButton *addButton;

@property(strong, retain)NSString *logoutFlag;
@property (weak, nonatomic) IBOutlet UIView *loaderView;
@property(strong, retain)NSString *liveFlag;

@end
