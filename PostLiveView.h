//
//  PostLiveView.h
//  SBN
//
//  Created by Sathish on 19/05/17.
//  Copyright © 2017 Optisol Business Solution. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PostLiveViewDelegate <NSObject>

- (void)didDoneButtonTap;

@end

@interface PostLiveView : UIView

@property(weak,nonatomic)IBOutlet UILabel *viewersCount;
@property(strong,nonatomic)IBOutlet UILabel *middleMessage;

@property(strong,nonatomic) IBOutlet UIButton *doneBtn;
@property(assign) id<PostLiveViewDelegate> delegate;
@end
