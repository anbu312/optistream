//
//  CreatePostViewController.h
//  StarBe
//
//  Created by Mac09 on 27/10/16.
//  Copyright © 2016 Optisol Business Solution Pvt Ltd
 
//

#import "BaseViewController.h"
#import <UIFloatLabelTextField.h>
#import <CoreLocation/CoreLocation.h>
#import <Foundation/Foundation.h>
#import "PhotoTweaksViewController.h"

@interface CreateLivePostViewController : BaseViewController<PhotoTweaksViewControllerDelegate, GMImagePickerControllerDelegate, AlertServiceDelegate, UITextFieldDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,CLLocationManagerDelegate,CLLocationManagerDelegate> {
    
    NSMutableArray *albumFileArray, *photoFileArray, *videoFileArray, *fileNameArray;
    BOOL isFree;
    int photo,video;
    AWSS3TransferManagerUploadRequest *upload;
}

@property(strong, retain)UINavigationController *mainViewController;
@property (weak, nonatomic) NSString *otherUserName;
@property (weak, nonatomic) NSString *otherUserId;
@property (weak, nonatomic) NSString *caputureMode;

@property (weak, nonatomic) IBOutlet UILabel *gallerInfoLbl;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;

@property (weak, nonatomic) IBOutlet UIFloatLabelTextField *txtHost;
@property (weak, nonatomic) IBOutlet UIFloatLabelTextField *txtTitle;
@property (weak, nonatomic) IBOutlet UIFloatLabelTextField *txtAddDesc;

@property (weak, nonatomic) IBOutlet UIButton *btnAddPhotos;
@property (weak, nonatomic) IBOutlet UIButton *btnPostNow;
@property (weak, nonatomic) IBOutlet UIButton *btnSchedulePost;


- (IBAction)postNow:(id)sender;
- (IBAction)addPhotos:(id)sender;

@end
