//  EnlargePicViewController.m
//  Hype
//
//  Created by Sathish on 22/03/16.
//  Copyright © 2016 Optisol Business Solution
//

#import "EnlargeRecordViewController.h"
#import "ProfileViewController.h"
#import "UIImageView+WebCache.h"
#import "LikeManager.h"
#import "Post.h"
#import "DetailedPostViewController.h"
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <Social/Social.h>
#import "FeedPost.h"
#import "CustomYesNoAlert.h"
#import "KLCPopup.h"
#import "HomeViewController.h"
#import "LiveListViewController.h"

@interface EnlargeRecordViewController ()<AVPlayerViewControllerDelegate>{
    NSString * reportTest;
    NSMutableArray * dict;
   // NSTimer *trackTimer;

}

@property (nonatomic) IBOutlet UIButton *playButton;
@property (strong, nonatomic) AVPlayerViewController *playerViewController;
@property (strong, nonatomic) AVPlayer *player;
@property KLCPopup* popup;

@end

@implementation EnlargeRecordViewController

@synthesize carousel,playBtn,scrollView,currentPostId,likeButton,countNumber,commentsLabel,commentButton,shareButton,viewsLabel,titleLabel,pageFlag,reportIcon;

-(void)viewWillDisappear:(BOOL)animated{
//    if ([trackTimer isValid]) {
//        [trackTimer invalidate];
//    }
//    trackTimer = nil;

    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [AppDelegate restrictRotation:YES];
}
- (void)viewDidDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [AppDelegate restrictRotation:YES];
}
-(void)viewDidLayoutSubviews{
    if([[UIDevice currentDevice] orientation] == UIDeviceOrientationPortrait){
        [[self navigationController] setNavigationBarHidden:NO animated:YES];
    }else{
        [[self navigationController] setNavigationBarHidden:YES animated:YES];
    }
}
- (void)viewDidLoad {
    [super viewDidLoad];
//    trackTimer = [NSTimer scheduledTimerWithTimeInterval:1.0f
//                                                  target:self
//                                                selector:@selector(videoOrientationAction)
//                                                userInfo:nil
//                                                 repeats:YES];

    [[NSNotificationCenter defaultCenter] removeObserver:self];
    dict = [[NSMutableArray alloc] init];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:[self.player currentItem]];
    [self.player.currentItem addObserver:self forKeyPath:@"status" options:NSKeyValueObservingOptionInitial context:nil];
    
}
-(void)videoOrientationAction{
//    if([[UIDevice currentDevice] orientation] == UIDeviceOrientationPortrait){
//        [[self navigationController] setNavigationBarHidden:NO animated:YES];
//    }else{
//        [[self navigationController] setNavigationBarHidden:YES animated:YES];
//    }
}
-(void)videoPlayAction{
    [AppDelegate restrictRotation:NO];
    self.player = [AVPlayer playerWithURL:[NSURL URLWithString:_liveUrl]];
    self.playerViewController = [AVPlayerViewController new];
    self.playerViewController.player = self.player;
    self.playerViewController.player.actionAtItemEnd = AVPlayerActionAtItemEndPause;
    
    [self.playerViewController.view setFrame:self.carousel.frame];
    [self.carousel addSubview: self.playerViewController.view];
    [self.carousel bringSubviewToFront:self.playerViewController.view];
    self.playerViewController.showsPlaybackControls = true;
    [[AVAudioSession sharedInstance]
     setCategory: AVAudioSessionCategoryPlayback
     error: nil];
    [self.playerViewController.player play];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationItem.title = _liveTitle;
    self.user = [[User alloc] initWithString:[Lib reteriveData:Access_Token] error:nil];
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"back_icon.png"] imageWithRenderingMode: UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(navigationBackButtonAction:)];
    self.navigationItem.leftBarButtonItem = barButtonItem;
    self.serviceDelegate.delegate = self;
    self.appUtil.alrDelegate = self;
    [self.serviceDelegate resetRKObjectManager];
    [self videoPlayAction];

}
-(IBAction)backAction:(id)sender{
    [AppDelegate restrictRotation:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self.player pause];
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)navigationBackButtonAction:(id)sender{
    NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationPortrait];
    [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self.player pause];
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)playerItemDidReachEnd:(NSNotification *)notification {
    //[self.player pause];
    [self.player seekToTime:kCMTimeZero];
}
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object
                        change:(NSDictionary *)change context:(void *)context {
    if([keyPath isEqualToString:@"status"]){
        
    }
    
    if (!self.player)
    {
        return;
    }
}
@end
