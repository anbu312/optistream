//
//  VideoPlayerViewController.m
//  SDKSampleApp
//
//  This code and all components (c) Copyright 2015-2016, Wowza Media Systems, LLC. All rights reserved.
//  This code is licensed pursuant to the BSD 3-Clause License.
//


#import <WowzaGoCoderSDK/WowzaGoCoderSDK.h>

#import "VideoPlayerViewController.h"
#import "SettingsViewModel.h"
#import "SettingsViewController.h"
#import "MP4Writer.h"
#import "LiveListViewController.h"
#import "KLCPopup.h"
#import "PostLiveView.h"
#import "ChatViewBottomContainer.h"
#import "LiveViewersProfileViewController.h"
#import "ChatViewController.h"
#import <CallKit/CXCallObserver.h>

#pragma mark VideoPlayerViewController (GoCoder SDK Sample App) -

NSString *const SDKSampleSavedConfigKey = @"SDKSampleSavedConfigKey";
NSString *const SDKSampleAppLicenseKey = @"GOSK-6943-0100-744D-509C-EC90";

NSString *const liveEndMessage = @"Fantastic! You went live for %@. Please be patience, your video will be available soon in your Event List so that more people can watch it. ";

//NSString *const SDKSampleAppLicenseKey = @"GOSK-8443-0103-6E4E-1F66-9289";

@interface VideoPlayerViewController () <WZStatusCallback, WZVideoSink, WZAudioSink, WZVideoEncoderSink, WZAudioEncoderSink, WZDataSink,PostLiveViewDelegate,UIPageViewControllerDelegate,UIPageViewControllerDataSource,UIGestureRecognizerDelegate,ChatViewControllerDelegate, AppLifeCycleDelegate, CXCallObserverDelegate>{
    NSTimer *_timer;
    NSMutableArray *tempUserDict;
    NSTimer *_watchingTimer;
    int checkState;
    NSString *watchUserCount;
    NSString *watchedUsersCount;
    NSString *tempTwillioToken;
    IBOutlet ChatViewBottomContainer *chatViewBottomContainer;
}

#pragma mark - UI Elements
@property (nonatomic, weak) IBOutlet UIButton           *watchingButton;
@property (nonatomic, weak) IBOutlet UIButton           *broadcastButton;
@property (nonatomic, weak) IBOutlet UIButton           *settingsButton;
@property (nonatomic, weak) IBOutlet UIButton           *switchCameraButton;
@property (nonatomic, weak) IBOutlet UIButton           *torchButton;
@property (nonatomic, weak) IBOutlet UIButton           *micButton;
@property (weak, nonatomic) IBOutlet UILabel            *timeLabel;
@property (weak, nonatomic) IBOutlet UIButton           *pingButton;

#pragma mark - GoCoder SDK Components
@property (nonatomic, strong) WowzaGoCoder      *goCoder;
@property (nonatomic, strong) WowzaConfig       *goCoderConfig;
@property (nonatomic, strong) WZCameraPreview   *goCoderCameraPreview;

#pragma mark - Data
@property (nonatomic, strong) NSMutableArray    *receivedGoCoderEventCodes;
@property (nonatomic, assign) BOOL              blackAndWhiteVideoEffect;
@property (nonatomic, assign) BOOL              recordVideoLocally;
@property (nonatomic, assign) CMTime            broadcastStartTime;

#pragma mark - MP4Writing
@property (nonatomic, strong) MP4Writer         *mp4Writer;
@property (nonatomic, assign) BOOL              writeMP4;
@property (nonatomic, strong) dispatch_queue_t  video_capture_queue;

#pragma mark - WZData injection
@property (nonatomic, assign) long long         broadcastFrameCount;

@property SettingsViewModel *livesettingModel;
@property KLCPopup* popup;


@property (nonatomic, strong, nonnull) SettingsViewModel *viewModel;
@property (nonatomic, strong, nonnull) PostLiveView *endLiveView;

// Chat properties
@property (nonatomic,strong) ChatViewController *chatViewController;
@property (nonatomic,strong) LiveViewersProfileViewController *liveViewersProfileVC;
@property (nonatomic,strong) UIPageViewController *PageViewController;
@property (nonatomic,strong) NSArray *arrPageTitles;

@property BOOL isOrientationLocked;
@property BOOL isStreamInterrupted;
@property UIInterfaceOrientation selectedOrientation;
@property (nonatomic, strong) CXCallObserver *callObserver;

// Chat methods
- (UIViewController *)viewControllerAtIndex:(NSUInteger)index;

@end

#pragma mark -
@implementation VideoPlayerViewController
@synthesize liveHostAddress,liveStreamId,livePortNumber,liveStreamName,liveApplicationName,liveUserName,livePassword;

#pragma mark - UIViewController Protocol Instance Methods

- (void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    NSLog(@"VideoPlayerViewController - goodbye");
}

- (void)twillioTokenAction:(NSNotification *)notification {
    NSDictionary* userInfo = notification.userInfo;
    tempTwillioToken = [userInfo objectForKey:@"twillio-data"];
    [self connectTwilioChatClient]; // Connecting Twilio Chat
}

-(void)streamEndedScreenNaviation {
    if([self.timeLabel.text isEqualToString:@"00:00"]){ // No connection started
        LiveListViewController *logObj =[[LiveListViewController alloc] initWithNibName:@"LiveListViewController" bundle:nil];
        logObj.mainViewController = self.navigationController;
        [self showViewController:logObj sender:nil];
    }else{
        [self showEndingScreen]; // Show ending screen here
    }
}

-(void)didEnterForeground {
    if (self.isStreamInterrupted) {
        [self streamEndedScreenNaviation];
        self.isStreamInterrupted = NO;
    }
}

- (void) viewDidLoad {
    [super viewDidLoad];
    
    self.callObserver = [[CXCallObserver alloc]init];
    [self.callObserver setDelegate:self queue:nil];
    
    [AppDelegate restrictRotation:YES];
    ((AppDelegate *)[UIApplication sharedApplication].delegate).lifeCycleDelegate = self;
    
    [self.chatBtn setUserInteractionEnabled:NO];
    [self.chatBtn setImage:[UIImage imageNamed:@"chat-outline"] forState:UIControlStateNormal];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(twillioTokenAction:)
                                                 name:@"TwillioNotificationCall"
                                               object:nil];
    
    self.user = [[User alloc] initWithString:[Lib reteriveData:Access_Token] error:nil];
    [_liveImage setHidden:YES];
    checkState = 0;
    
    self.watchingButton.hidden = YES;
    self.watchingButton.userInteractionEnabled = NO;
    
    tempUserDict = [[NSMutableArray alloc] init];
    
    self.blackAndWhiteVideoEffect = [[NSUserDefaults standardUserDefaults] boolForKey:BlackAndWhiteKey];
    self.recordVideoLocally = [[NSUserDefaults standardUserDefaults] boolForKey:RecordVideoLocallyKey];
    self.broadcastStartTime = kCMTimeInvalid;
    self.timeLabel.hidden = YES;
    
    self.receivedGoCoderEventCodes = [NSMutableArray new];
    [WowzaGoCoder setLogLevel:WowzaGoCoderLogLevelDefault];
    
    // Load or initialization the streaming configuration settings
    NSData *savedConfig = [[NSUserDefaults standardUserDefaults] objectForKey:SDKSampleSavedConfigKey];
    if (savedConfig) {
        self.goCoderConfig = [NSKeyedUnarchiver unarchiveObjectWithData:savedConfig];
        self.goCoderConfig.hostAddress = liveHostAddress;
        self.goCoderConfig.applicationName = liveApplicationName;
        self.goCoderConfig.streamName = liveStreamName;
        self.goCoderConfig.broadcastScaleMode = WZBroadcastScaleModeAspectFit;
        self.goCoderConfig.videoWidth = 1280;
        self.goCoderConfig.videoHeight = 720;
        self.goCoderConfig.videoBitrate = 750000;
        self.goCoderConfig.portNumber = 1935;
        self.goCoderConfig.capturedVideoRotates = NO;
        self.goCoderConfig.videoPreviewRotates = NO;
        self.goCoderConfig.broadcastVideoOrientation = WZBroadcastOrientationAlwaysPortrait;
        
    } else {
        self.goCoderConfig = [WowzaConfig new];
        self.goCoderConfig.hostAddress = liveHostAddress;
        self.goCoderConfig.applicationName = liveApplicationName;
        self.goCoderConfig.streamName = liveStreamName;
        self.goCoderConfig.broadcastScaleMode = WZBroadcastScaleModeAspectFit;
        self.goCoderConfig.videoWidth = 1280;
        self.goCoderConfig.videoHeight = 720;
        self.goCoderConfig.videoBitrate = 750000;//3750000
        self.goCoderConfig.portNumber = 1935;
        self.goCoderConfig.capturedVideoRotates = NO;
        self.goCoderConfig.videoPreviewRotates = NO;
        self.goCoderConfig.broadcastVideoOrientation = WZBroadcastOrientationAlwaysPortrait;
        
    }
    
    NSLog (@"WowzaGoCoderSDK version =\n major:%lu\n minor:%lu\n revision:%lu\n build:%lu\n short string: %@\n verbose string: %@",
           (unsigned long)[WZVersionInfo majorVersion],
           (unsigned long)[WZVersionInfo minorVersion],
           (unsigned long)[WZVersionInfo revision],
           (unsigned long)[WZVersionInfo buildNumber],
           [WZVersionInfo string],
           [WZVersionInfo verboseString]);
    
    NSLog (@"%@", [WZPlatformInfo string]);
    
    self.goCoder = nil;
    
    // Register the GoCoder SDK license key
    NSError *goCoderLicensingError = [WowzaGoCoder registerLicenseKey:SDKSampleAppLicenseKey];
    if (goCoderLicensingError != nil) {
        // Handle license key registration failure
        [VideoPlayerViewController showAlertWithTitle:@"GoCoder SDK Licensing Error" error:goCoderLicensingError presenter:self];
    }
    else {
        // Initialize the GoCoder SDK
        self.goCoder = [WowzaGoCoder sharedInstance];
        
        // Specify the view in which to display the camera preview
        if (self.goCoder != nil) {
            
            // Request camera and microphone permissions
            [WowzaGoCoder requestPermissionForType:WowzaGoCoderPermissionTypeCamera response:^(WowzaGoCoderCapturePermission permission) {
                NSLog(@"Camera permission is: %@", permission == WowzaGoCoderCapturePermissionAuthorized ? @"authorized" : @"denied");
            }];
            
            [WowzaGoCoder requestPermissionForType:WowzaGoCoderPermissionTypeMicrophone response:^(WowzaGoCoderCapturePermission permission) {
                NSLog(@"Microphone permission is: %@", permission == WowzaGoCoderCapturePermissionAuthorized ? @"authorized" : @"denied");
            }];
            
            [self.goCoder registerVideoSink:self];
            [self.goCoder registerAudioSink:self];
            [self.goCoder registerVideoEncoderSink:self];
            [self.goCoder registerAudioEncoderSink:self];
            
            [self.goCoder registerDataSink:self eventName:@"onTextData"];
            
            self.goCoder.config = self.goCoderConfig;
            self.goCoder.cameraView = self.view;
            
            // Start the camera preview
            self.goCoderCameraPreview = self.goCoder.cameraPreview;
            [self.goCoderCameraPreview startPreview];
            //            [self didTapBroadcastButton:nil];
            
        }
        
        // Update the UI controls
        [self updateUIControls];
    }
    NSLog(@"Live ID....... %@",liveStreamId);
    
    [self startStreamAction:@"Start"];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationChanged:) name:UIDeviceOrientationDidChangeNotification object:[UIDevice currentDevice]];
    
    [self chatUIConfiguratoin];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    //[self askUserForOrientationLock];
}

- (void)chatUIConfiguratoin{
    _arrPageTitles = @[@"IPChatView"];
    
    // Create a new inner view controllers
    _chatViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ChatViewControllerID"];
    //_liveViewersProfileVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LiveViewersProfileViewControllerID"];
    
    // Set delegate
    _chatViewController.chatViewDelegate = self;
    
    // Re-framing both inner view controllers
    [_chatViewController setFrame:chatViewBottomContainer];
    //_liveViewersProfileVC.view.frame = _chatViewController.view.frame;
    
    // Set page index
    _chatViewController.pageIndex = 0;
    //_liveViewersProfileVC.pageIndex = 1;
    
    // Create page view controller
    self.PageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ChatPagerID"];
    self.PageViewController.dataSource = self;
    
    NSArray *viewControllers = @[_chatViewController];
    [self.PageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    // Change the size of page view controller
    self.PageViewController.view.frame = CGRectMake(0, 0, chatViewBottomContainer.frame.size.width, chatViewBottomContainer.frame.size.height);
    
    // Add it into bottom container
    [chatViewBottomContainer addSubview:self.PageViewController.view];
    [self.PageViewController didMoveToParentViewController:self];
    
    [self getChatToken:liveStreamId device:[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"deviceToken"]]];
}

-(void)startStreamAction:(NSString *)flag {
    ResponseHandler callback = ^(BOOL wasSuccessful, RKMappingResult *mappingResult,RKObjectRequestOperation *operation) {
        DLOG(@"callback reponse %@",operation.HTTPRequestOperation.responseString);
        if(operation.HTTPRequestOperation.response.statusCode == 200){
            if([flag isEqualToString:@"Stop"]){
                
                if ([_watchingTimer isValid]) {
                    [_watchingTimer invalidate];
                }
                
                _watchingTimer = nil;
                
                if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateActive) {
                    [self streamEndedScreenNaviation];
                }
                
            }else if([flag isEqualToString:@"Start"]){
                [self didTapBroadcastButton:nil];
            }
        }
    };
    NSString *tempUrl;
    if([flag isEqualToString:@"Start"]){
        tempUrl= [NSString stringWithFormat:@"%@%@",START_LIVE_STREAM,liveStreamId];
    }else{
        tempUrl= [NSString stringWithFormat:@"%@%@",STOP_LIVE_STREAM,liveStreamId];
    }
    [self.serviceDelegate executePostLinkedURL:tempUrl param:nil responseMapping:self.serviceDelegate.dummyMapping keyPath:nil callBack:callback onView:self.view];
}
-(void)singleAlertBtnClicked{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    LiveListViewController *logObj =[[LiveListViewController alloc] initWithNibName:@"LiveListViewController" bundle:nil];
    logObj.mainViewController = self.navigationController;
    [self showViewController:logObj sender:nil];
}

- (void) viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    self.goCoder.cameraPreview.previewLayer.frame = self.view.bounds;
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    
    NSData *savedConfigData = [NSKeyedArchiver archivedDataWithRootObject:self.goCoderConfig];
    [[NSUserDefaults standardUserDefaults] setObject:savedConfigData forKey:SDKSampleSavedConfigKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    // Update the configuration settings in the GoCoder SDK
    if (self.goCoder != nil)
        self.goCoder.config = self.goCoderConfig;
    
    self.blackAndWhiteVideoEffect = [[NSUserDefaults standardUserDefaults] boolForKey:BlackAndWhiteKey];
    self.recordVideoLocally = [[NSUserDefaults standardUserDefaults] boolForKey:RecordVideoLocallyKey];
    
    self.serviceDelegate.delegate = self;
    self.appUtil.alrDelegate = self;
    [self.serviceDelegate resetRKObjectManager];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:nil];
    
}
- (void) didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (BOOL) prefersStatusBarHidden {
    return YES;
}

#pragma mark - Button Actions
- (IBAction)backBtnAction:(id)sender {
    //[self.navigationController popViewControllerAnimated:YES];
    [self startStreamAction:@"Stop"];
}

-(void)userAlert{
    UserListView  *customAlert= [[[NSBundle mainBundle] loadNibNamed:@"UserListView"
                                                               owner:nil
                                                             options:nil] objectAtIndex:0];
    customAlert.tempUserTableView.delegate = customAlert;
    customAlert.tempUserTableView.dataSource= customAlert;
    customAlert.layer.masksToBounds = true;
    customAlert.clipsToBounds = true;
    customAlert.tempUserDic = tempUserDict;
    customAlert.tempFlag = @"watching";
    customAlert.pageFlag = @"Feed";
    
    KLCPopupLayout layout = KLCPopupLayoutMake(KLCPopupHorizontalLayoutCenter,
                                               (KLCPopupVerticalLayout)KLCPopupVerticalLayoutCenter);
    
    _popup = [KLCPopup popupWithContentView:customAlert
                                   showType:KLCPopupShowTypeFadeIn
                                dismissType:KLCPopupDismissTypeFadeOut
                                   maskType:KLCPopupMaskTypeDimmed
                   dismissOnBackgroundTouch:YES
                      dismissOnContentTouch:NO];
    [_popup showWithLayout:layout];
}

#pragma mark - UI Action Methods
-(IBAction)watchingUserList:(id)sender{
    ResponseHandler callback = ^(BOOL wasSuccessful, RKMappingResult *mappingResult,RKObjectRequestOperation *operation) {
        DLOG(@"callback reponse %@",operation.HTTPRequestOperation.responseString);
        tempUserDict=nil;
        if(operation.HTTPRequestOperation.response.statusCode == 200){
            tempUserDict = [NSJSONSerialization JSONObjectWithData:[operation.HTTPRequestOperation.responseString dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
            [self userAlert];
        }else if(operation.HTTPRequestOperation.response.statusCode == 206){
            [self simpleAlert:@"No user data" withDelegate:self.view];
        }
    };
    NSString *tempUrl = [NSString stringWithFormat:@"%@%@",WATCH_LIST,liveStreamId];
    [self.serviceDelegate executeGetURLNotLoading:tempUrl param:nil responseMapping:self.serviceDelegate.dummyMapping keyPath:nil callBack:callback onView:self.view];
}

-(void)watchList{
    ResponseHandler callback = ^(BOOL wasSuccessful, RKMappingResult *mappingResult,RKObjectRequestOperation *operation) {
        DLOG(@"callback reponse %@",operation.HTTPRequestOperation.responseString);
        if(operation.HTTPRequestOperation.response.statusCode == 200){
            NSDictionary *userCount = [NSJSONSerialization JSONObjectWithData:[operation.HTTPRequestOperation.responseString dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
            watchUserCount = [NSString stringWithFormat:@"  %@",[userCount valueForKey:@"watchUsersCount"]];
            watchedUsersCount = [NSString stringWithFormat:@"  %@",[userCount valueForKey:@"watchedUsersCount"]];
            [self.watchingButton setTitle:watchUserCount forState:UIControlStateNormal];
        }
    };
    NSString *tempUrl= [NSString stringWithFormat:@"%@%@?isCount=%@",WATCH_LIST,liveStreamId,@"true"];
    [self.serviceDelegate executeGetURLNotLoading:tempUrl param:nil responseMapping:self.serviceDelegate.dummyMapping keyPath:nil callBack:callback onView:self.view];
}

- (IBAction) didTapBroadcastButton:(id)sender {
    NSError *configError = [self.goCoder.config validateForBroadcast];
    if (configError != nil) {
        [VideoPlayerViewController showAlertWithTitle:@"Incomplete Streaming Settings" error:configError presenter:self];
        return;
    }
    
    // Disable the U/I controls
    dispatch_async(dispatch_get_main_queue(), ^{
        self.broadcastButton.enabled    = NO;
        self.broadcastButton.hidden     = YES;
        self.torchButton.enabled        = NO;
        self.switchCameraButton.enabled = NO;
        self.settingsButton.enabled     = NO;
    });
    
    if (self.goCoder.status.state == WZStateRunning) {
        [_liveImage setHidden:YES];
        [self startStreamAction:@"Stop"];
        [self.goCoder endStreaming:self];
        [UIApplication sharedApplication].idleTimerDisabled = NO;
    }else{
        [self.receivedGoCoderEventCodes removeAllObjects];
        [self.goCoder startStreaming:self];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.micButton setImage:[UIImage imageNamed:(self.goCoder.isAudioMuted ? @"mute-live" : @"unmute_live")] forState:UIControlStateNormal];
            [UIApplication sharedApplication].idleTimerDisabled = YES;
        });
    }
}

- (IBAction) didTapSwitchCameraButton:(id)sender {
    WZCamera *otherCamera = [self.goCoderCameraPreview otherCamera];
    if (![otherCamera supportsWidth:self.goCoderConfig.videoWidth]) {
        [self.goCoderConfig loadPreset:otherCamera.supportedPresetConfigs.lastObject.toPreset];
        self.goCoder.config = self.goCoderConfig;
    }
    [self.goCoderCameraPreview switchCamera];
    [self.torchButton  setImage:[UIImage imageNamed:@"flash-live"] forState:UIControlStateNormal];
    [self updateUIControls];
}

- (IBAction) didTapTorchButton:(id)sender {
    BOOL newTorchOnState = !self.goCoderCameraPreview.camera.torchOn;
    
    self.goCoderCameraPreview.camera.torchOn = newTorchOnState;
    [self.torchButton setImage:[UIImage imageNamed:(newTorchOnState ? @"flash-off" : @"flash-live")] forState:UIControlStateNormal];
}

- (IBAction) didTapMicButton:(id)sender {
    BOOL newMutedState = !self.goCoder.isAudioMuted;
    
    self.goCoder.audioMuted = newMutedState;
    [self.micButton setImage:[UIImage imageNamed:(newMutedState ? @"mute-live" : @"unmute_live")] forState:UIControlStateNormal];
}

- (IBAction) didTapSettingsButton:(id)sender {
    UIViewController *settingsNavigationController = [[UIStoryboard storyboardWithName:@"GoCoderSettings" bundle:nil] instantiateViewControllerWithIdentifier:@"settingsNavigationController"];
    
    SettingsViewController *settingsVC = (SettingsViewController *)(((UINavigationController *)settingsNavigationController).topViewController);
    [settingsVC addAllSections];
    
    SettingsViewModel *settingsModel = [[SettingsViewModel alloc] initWithSessionConfig:self.goCoderConfig];
    settingsModel.supportedPresetConfigs = self.goCoder.cameraPreview.camera.supportedPresetConfigs;
    settingsVC.viewModel = settingsModel;
    [self presentViewController:settingsNavigationController animated:YES completion:NULL];
}

- (IBAction) didTapPingButton:(id)sender {
    /*
     The "Ping" button exists in order to demonstrate making a server function call; in this case, the call
     is "onGetPingTime". The server module that implements "onGetPingTime" must exist in order to receive
     a callback.
     */
    if (self.goCoder.status.state == WZStateRunning) {
        WZDataMap *params = [WZDataMap new];
        [self.goCoder sendDataEvent:WZDataScopeModule eventName:@"onGetPingTime" params:params callback:^(WZDataMap * _Nullable result, BOOL isError) {
            if (!isError && result) {
                WZDataItem *item = [result.data valueForKey:@"pingTime"];
                if (item) {
                    NSLog(@"onGetPingTime result - ping time = %0.2f", item.doubleValue);
                }
            }
        }];
    }
}

#pragma mark - Notifications

- (void) orientationChanged:(NSNotification *)notification {
    
    UIDevice * device = notification.object;
    
    WZDataMap *params = [WZDataMap new];
    switch(device.orientation) {
        case UIDeviceOrientationPortrait:
            [params setString:@"portrait" forKey:@"deviceOrientation"];
            [params setInteger:0 forKey:@"deviceRotation"];
            break;
            
        case UIDeviceOrientationPortraitUpsideDown:
            [params setString:@"portrait" forKey:@"deviceOrientation"];
            [params setInteger:180 forKey:@"deviceRotation"];
            break;
            
        case UIDeviceOrientationLandscapeLeft:
            [params setString:@"landscape" forKey:@"deviceOrientation"];
            [params setInteger:90 forKey:@"deviceRotation"];
            break;
            
        case UIDeviceOrientationLandscapeRight:
            [params setString:@"landscape" forKey:@"deviceOrientation"];
            [params setInteger:270 forKey:@"deviceRotation"];
            break;
            
        default:
            break;
    };
    
    if (params.data.count > 0) {
        [self.goCoder sendDataEvent:WZDataScopeStream eventName:@"onDeviceOrientation" params:params callback:nil];
    }
    
}

#pragma mark - Instance Methods

// Update the state of the UI controls
- (void) updateUIControls {
    if (self.goCoder.status.state != WZStateIdle && self.goCoder.status.state != WZStateRunning) {
        // If a streaming broadcast session is in the process of starting up or shutting down,
        // disable the UI controls
        self.broadcastButton.enabled    = NO;
        self.torchButton.enabled        = NO;
        self.switchCameraButton.enabled = NO;
        self.settingsButton.enabled     = NO;
        self.micButton.hidden           = YES;
        self.micButton.enabled          = NO;
        self.pingButton.hidden          = YES;
    } else {
        // Set the UI control state based on the streaming broadcast status, configuration,
        // and device capability
        self.broadcastButton.enabled    = YES;
        self.switchCameraButton.enabled = self.goCoderCameraPreview.cameras.count > 1;
        self.torchButton.enabled        = [self.goCoderCameraPreview.camera hasTorch];
        self.settingsButton.enabled     = !self.goCoder.isStreaming;
        // The mic icon should only be displayed while streaming and audio streaming has been enabled
        // in the GoCoder SDK configuration setiings
        self.micButton.enabled          = self.goCoder.isStreaming && self.goCoderConfig.audioEnabled;
        self.micButton.hidden           = !self.micButton.enabled;
        self.pingButton.hidden          = !self.goCoder.isStreaming;
    }
}

#pragma mark - WZStatusCallback Protocol Instance Methods
- (void) onWZStatus:(WZStatus *) goCoderStatus {
    // A successful status transition has been reported by the GoCoder SDK
    
    switch (goCoderStatus.state) {
            
        case WZStateIdle:
            // [_connectLbl setHidden:NO];
            self.timeLabel.hidden = YES;
            [_liveImage setHidden:YES];
            if (self.writeMP4 && self.mp4Writer.writing) {
                if (self.video_capture_queue) {
                    dispatch_async(self.video_capture_queue, ^{
                        [self.mp4Writer stopWriting];
                    });
                }
                else {
                    [self.mp4Writer stopWriting];
                }
            }
            self.writeMP4 = NO;
            break;
            
        case WZStateStarting:
            [self.broadcastButton setUserInteractionEnabled:NO];
            [_connectLbl setHidden:NO];
            [_titleLbl setHidden:YES];
            // A streaming broadcast session is starting up
            self.broadcastStartTime = kCMTimeInvalid;
            self.timeLabel.text = @"00:00";
            self.broadcastFrameCount = 0;
            break;
            
        case WZStateRunning:
            [self liveNotifiy];
            [self timerDataLoad];
            self.watchingButton.hidden = NO;
            self.broadcastButton.hidden = NO;
            self.watchingButton.userInteractionEnabled = YES;
            [self.broadcastButton setUserInteractionEnabled:YES];
            [_connectLbl setHidden:YES];
            [_liveImage setHidden:NO];
            // A streaming broadcast session is running
            self.timeLabel.hidden = NO;
            
            [self enableUserChat];
            
            self.writeMP4 = NO;
            if (self.recordVideoLocally) {
                self.mp4Writer = [MP4Writer new];
                self.writeMP4 = [self.mp4Writer prepareWithConfig:self.goCoderConfig];
                if (self.writeMP4) {
                    [self.mp4Writer startWriting];
                }
            }
            break;
            
        case WZStateStopping:
            // A streaming broadcast session is shutting down
            break;
            
        default:
            break;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self updateUIControls];
    });
}

- (void) onWZEvent:(WZStatus *) goCoderStatus {
    // If an event is reported by the GoCoder SDK, display an alert dialog describing the event,
    // but only if we haven't already shown an alert for this event
    
    dispatch_async(dispatch_get_main_queue(), ^{
        __block BOOL haveSeenAlertForEvent = NO;
        [self.receivedGoCoderEventCodes enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([((NSNumber *)obj) isEqualToNumber:[NSNumber numberWithInteger:goCoderStatus.error.code]]) {
                haveSeenAlertForEvent = YES;
                *stop = YES;
            }
        }];
        
        if (!haveSeenAlertForEvent) {
            [VideoPlayerViewController showAlertWithTitle:@"Live Streaming Event" status:goCoderStatus presenter:self];
            [self.receivedGoCoderEventCodes addObject:[NSNumber numberWithInteger:goCoderStatus.error.code]];
        }
        
        [self updateUIControls];
    });
}
-(void)timerDataLoad{
    _watchingTimer = [NSTimer scheduledTimerWithTimeInterval:1.0f
                                                      target:self
                                                    selector:@selector(watchList)
                                                    userInfo:nil
                                                     repeats:YES];
}
-(void)changeMessage{
    _connectLbl.text = @"Connecting Live...";
    
    _timer = [NSTimer scheduledTimerWithTimeInterval:1.0f
                                              target:self
                                            selector:@selector(didTapBroadcastButton:)
                                            userInfo:nil
                                             repeats:NO];
}

- (void) onWZError:(WZStatus *) goCoderStatus {
    // If an error is reported by the GoCoder SDK, display an alert dialog containing the error details
    dispatch_async(dispatch_get_main_queue(), ^{
        if(goCoderStatus.state == WZStateIdle){
            [self performSelector:@selector(changeMessage)  withObject:nil afterDelay:2.0];
            //[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(didTapBroadcastButton:) userInfo:nil repeats:NO];
        }else{
            [VideoPlayerViewController showAlertWithTitle:@"Live Streaming Error" status:goCoderStatus presenter:self];
        }
        [self updateUIControls];
    });
}

-(void)liveNotifiy{
    if ([_timer isValid]) {
        [_timer invalidate];
    }
    _timer = nil;
    ResponseHandler callback = ^(BOOL wasSuccessful, RKMappingResult *mappingResult,RKObjectRequestOperation *operation) {
        DLOG(@"callback reponse %@",operation.HTTPRequestOperation.responseString);
        if(operation.HTTPRequestOperation.response.statusCode == 200){
        }
    };
    NSString *tempUrl= [NSString stringWithFormat:@"%@%@",NOTIFY_LIVE_STREAM,liveStreamId];
    [self.serviceDelegate executePostLinkedURL:tempUrl param:nil responseMapping:self.serviceDelegate.dummyMapping keyPath:nil callBack:callback onView:self.view];
}

// warning Don't implement this protocol unless your application makes use of it
- (void) videoFrameWasCaptured:(nonnull CVImageBufferRef)imageBuffer framePresentationTime:(CMTime)framePresentationTime frameDuration:(CMTime)frameDuration {
    if (self.goCoder.isStreaming) {
        
        if (self.blackAndWhiteVideoEffect) {
            // convert frame to b/w using CoreImage tonal filter
            CIImage *frameImage = [[CIImage alloc] initWithCVImageBuffer:imageBuffer];
            CIFilter *grayFilter = [CIFilter filterWithName:@"CIPhotoEffectTonal"];
            [grayFilter setValue:frameImage forKeyPath:@"inputImage"];
            frameImage = [grayFilter outputImage];
            
            CIContext * context = [CIContext contextWithOptions:nil];
            [context render:frameImage toCVPixelBuffer:imageBuffer];
        }
    }
}

-(void)callObserver:(CXCallObserver *)callObserver callChanged:(CXCall *)call {
    self.isStreamInterrupted = YES;
    [self startStreamAction:@"Stop"];
    [self.goCoder endStreaming:self];
}

- (void)videoCaptureInterruptionStarted {
    if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateBackground) {
        self.isStreamInterrupted = YES;
        [self startStreamAction:@"Stop"];
        [self.goCoder endStreaming:self];
    }
}

- (void) videoCaptureUsingQueue:(nullable dispatch_queue_t)queue {
    self.video_capture_queue = queue;
}

#pragma mark - WZAudioSink

//warning Don't implement this protocol unless your application makes use of it
- (void) audioLevelDidChange:(float)level {
    //    NSLog(@"%@ %0.2f", @"Audio level did change", level);
}

//warning Don't implement this protocol unless your application makes use of it
- (void) audioPCMFrameWasCaptured:(nonnull const AudioStreamBasicDescription *)pcmASBD bufferList:(nonnull const AudioBufferList *)bufferList time:(CMTime)time sampleRate:(Float64)sampleRate {
    // The commented-out code below simply dampens the amplitude of the audio data.
    // It is intended as an example of how one would access and modify the audio sample data.
    
    //    int16_t *fdata = bufferList->mBuffers[0].mData;
    //
    //    for (int i = 0; i < bufferList->mBuffers[0].mDataByteSize/sizeof(*fdata); i++) {
    //        *fdata = (int16_t)(*fdata * 0.1);
    //        fdata++;
    //    }
}


#pragma mark - WZAudioEncoderSink

//warning Don't implement this protocol unless your application makes use of it
- (void) audioSampleWasEncoded:(nullable CMSampleBufferRef)data {
    if (self.writeMP4) {
        [self.mp4Writer appendAudioSample:data];
    }
}


#pragma mark - WZVideoEncoderSink

//warning Don't implement this protocol unless your application makes use of it
- (void) videoFrameWasEncoded:(nonnull CMSampleBufferRef)data {
    
    // update the broadcast time label
    if (CMTimeCompare(self.broadcastStartTime, kCMTimeInvalid) == 0) {
        self.broadcastStartTime = CMSampleBufferGetPresentationTimeStamp(data);
    }
    else {
        CMTime diff = CMTimeSubtract(CMSampleBufferGetPresentationTimeStamp(data), self.broadcastStartTime);
        Float64 seconds = CMTimeGetSeconds(diff);
        NSInteger duration = (NSInteger)seconds;
        
        NSString *timeString = [NSString stringWithFormat:@"%02ld:%02ld", (long)(duration / 60), (long)(duration % 60)];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            self.timeLabel.text = timeString;
        });
    }
    
    if (self.writeMP4) {
        [self.mp4Writer appendVideoSample:data];
    }
}

// Adding End live view into camera view
- (void) showEndingScreen{
    self.endLiveView = [[[NSBundle mainBundle] loadNibNamed:@"PostLiveView" owner:self options:nil] objectAtIndex:0];
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    self.endLiveView.frame = CGRectMake(0, 0, screenRect.size.width, screenRect.size.height);
    
    self.endLiveView.delegate = self;
    self.endLiveView.viewersCount.text = [NSString stringWithFormat:@"%@ viewer(s)",watchedUsersCount];
    self.endLiveView.middleMessage.text = [NSString stringWithFormat:liveEndMessage,self.timeLabel.text];
    
    self.endLiveView.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.5];
    self.endLiveView.doneBtn.layer.cornerRadius = 3;
    self.endLiveView.doneBtn.layer.masksToBounds = YES;
    
    [self.view addSubview:self.endLiveView];
}

// End live view delegate
- (void)didDoneButtonTap{
    [self.endLiveView removeFromSuperview];
    LiveListViewController *logObj =[[LiveListViewController alloc] initWithNibName:@"LiveListViewController" bundle:nil];
    logObj.mainViewController = self.navigationController;
    [self showViewController:logObj sender:nil];
}

#pragma mark - WZDataSink
- (void) onData:(WZDataEvent *)dataEvent {
    NSLog(@"Got data - %@", dataEvent.description);
}

#pragma mark -
+ (void) showAlertWithTitle:(NSString *)title status:(WZStatus *)status presenter:(UIViewController *)presenter {
    
    [SettingsViewController presentAlert:title message:status.description presenter:presenter];
}

+ (void) showAlertWithTitle:(NSString *)title error:(NSError *)error presenter:(UIViewController *)presenter {
    
    [SettingsViewController presentAlert:title message:error.localizedDescription presenter:presenter];
}

#pragma mark - Live Chat Implementation
//################################# LIVE CHAT IMPLEMENTATION  #######################//

- (void)connectTwilioChatClient{
    NSString *tempUserId = [NSString stringWithFormat:@"%@",self.user.access_token.userInfo._id];
    [_chatViewController connectWithTwilioClient:tempUserId token:tempTwillioToken channel:liveStreamId];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index;
    
    if ([viewController isKindOfClass:ChatViewController.class]) {
        index = ((ChatViewController*) viewController).pageIndex;
    }else{
        index = ((LiveViewersProfileViewController*) viewController).pageIndex;
    }
    
    if ((index == 0) || (index == NSNotFound))
    {
        return nil;
    }
    
    index--;
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index;
    
    if ([viewController isKindOfClass:ChatViewController.class]) {
        index = ((ChatViewController*) viewController).pageIndex;
    }else{
        index = ((LiveViewersProfileViewController*) viewController).pageIndex;
    }
    
    if (index == NSNotFound)
    {
        return nil;
    }
    
    index++;
    if (index == [self.arrPageTitles count])
    {
        return nil;
    }
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)viewControllerAtIndex:(NSUInteger)index
{
    if (([self.arrPageTitles count] == 0) || (index >= [self.arrPageTitles count])) {
        return nil;
    }
    NSLog(@"index.... %ld",index);
    index == 0?(_chatViewController.pageIndex = index):(_liveViewersProfileVC.pageIndex = index);
    return index==0?_chatViewController:_liveViewersProfileVC;
}

-(IBAction)beginChat:(id)sender{
    NSArray *viewControllers = @[_chatViewController];
    [self.PageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionReverse animated:YES completion:nil];
    [self performSelector:@selector(bringInputBar) withObject:nil afterDelay:0.4];
}

-(IBAction)liveUsers:(id)sender{
    NSArray *viewControllers = @[_liveViewersProfileVC];
    [self.PageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
    [self performSelector:@selector(loadViewers) withObject:nil afterDelay:1];
}

- (void)loadViewers{
    [_liveViewersProfileVC refereshUserList];
}

-(void)bringInputBar{
    [_chatViewController showKeyBoardWithInputBar];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gesture shouldReceiveTouch:(UITouch *)touch {
    if (![touch.view.superview isKindOfClass:[UITableViewCell class]]){
        return YES;
    }
    return NO;
}

// Hide bottom container - By tap the view, can be used to hide/show all components when we are in live streaming
- (void)hideOrShowChatViewBottomContainer{
    chatViewBottomContainer.hidden = !chatViewBottomContainer.hidden;
}

-(void)enableUserChat{
    self.chatBtn.userInteractionEnabled = YES;
    [self.chatBtn setImage:[UIImage imageNamed:@"chat"] forState:UIControlStateNormal];
    
    //    self.chatBtn.titleLabel.textColor = [UIColor blackColor];
    //    self.chatBtn.backgroundColor = [UIColor whiteColor];
}

@end





