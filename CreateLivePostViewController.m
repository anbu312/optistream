//
//  CreatePostViewController.m
//  StarBe
//
//  Created by Mac09 on 27/10/16.
//  Copyright © 2016 Optisol Business Solution Pvt Ltd
 
//

#import "CreateLivePostViewController.h"
#import <RestKit/RKObjectManager.h>
#import "UIView+UpdateAutoLayoutConstraints.h"
#import "RMActionController.h"
#import "RMDateSelectionViewController.h"
#import "String.h"
#import "CustomYesNoAlert.h"
#import "KLCPopup.h"
#import "SettingsViewModel.h"
#import "VideoPlayerViewController.h"
#import "CoreLocation/CoreLocation.h"
#import "LiveListViewController.h"

@interface CreateLivePostViewController ()<UIImagePickerControllerDelegate>{
    BOOL isCameraAccess;
    BOOL isChooseCamera;
    BOOL isSchedule;
    UIImage *chooseImage;
    BOOL isVideo;
    NSURL *videoUrl;
    NSString * scheduleTime;
    NSDictionary *returnData;
    
    UIImagePickerController *setPicker;
    GMImagePickerController *setGMPicker;

//    CLLocation *currentLocation;
//    CLGeocoder *geocoder;
}
//@property (strong, nonatomic) CLLocationManager * locationmanager;

@property KLCPopup* popup;
@end

@implementation CreateLivePostViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _btnSchedulePost.layer.borderColor = [UIColor colorWithRed:246.0/255.0 green:184.0/255.0 blue:11.0/255.0 alpha:1.0].CGColor;
    _btnSchedulePost.layer.borderWidth = 1.0;

    returnData = [[NSDictionary alloc]init];
    NSError *err;
    self.user = [[User alloc] initWithString:[Lib reteriveData:Access_Token] error:&err];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(createLivePostInfo)
                                                 name:@"PostInfo"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(livePageAction)
                                                 name:@"LivePage"
                                               object:nil];
    //LIVE_STREAM_PAGE
    isChooseCamera = NO;
    isFree = true;
    // Do any additional setup after loading the view.
    fileNameArray = [[NSMutableArray alloc]init];
    photoFileArray = [[NSMutableArray alloc]init];
    videoFileArray = [[NSMutableArray alloc]init];
    albumFileArray = [[NSMutableArray alloc]init];
    
//    double currentLatitude = self.locationmanager.location.coordinate.latitude;
//    double currentLongitude = self.locationmanager.location.coordinate.longitude;
//    //Getting device's current location
//    currentLocation = [[CLLocation alloc]initWithLatitude:currentLatitude
//                                                longitude:currentLongitude];
//    [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error)
//     {
//         if (error){
//             NSLog(@"failed with error: %@", error);
//             return;
//         }
//         CLPlacemark *placemark = [placemarks objectAtIndex:0];
//         
//     }];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
}
-(void)showScheduleTime{
    RMActionControllerStyle style = RMActionControllerStyleWhite;
    RMAction *selectAction = [RMAction actionWithTitle:@"Select" style:RMActionStyleDone andHandler:^(RMActionController *controller) {
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:string_key_date_formatter_datetime];
        
        [self.btnSchedulePost setTitle:[NSString stringWithFormat:@"%@", [dateFormatter stringFromDate: ((UIDatePicker *)controller.contentView).date]] forState:UIControlStateNormal];
        
        [dateFormatter setDateFormat:string_key_time_formatter];
        NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:string_utc];
        [dateFormatter setTimeZone:timeZone];
        
        scheduleTime =[NSString stringWithFormat:@"%@", [dateFormatter stringFromDate: ((UIDatePicker *)controller.contentView).date]];
        
        [self scheduleConfirmation];
    }];
    
    //Create cancel action
    RMAction *cancelAction = [RMAction actionWithTitle:@"Close" style:RMActionStyleCancel andHandler:^(RMActionController *controller) {
    }];
    
    //Create date selection view controller
    RMDateSelectionViewController *dateSelectionController = [RMDateSelectionViewController actionControllerWithStyle:style selectAction:selectAction andCancelAction:cancelAction];
    dateSelectionController.title = @"";
    //  dateSelectionController.message = @"Select Baby's Date of birth.";
    dateSelectionController.datePicker.maximumDate = [NSDate date];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDate *currentDate = [NSDate date];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    //    [comps setYear:-2];
    [comps setDay:31];
    NSDate *maxDate = [calendar dateByAddingComponents:comps toDate:currentDate options:0];
    
    dateSelectionController.datePicker.minimumDate = [NSDate date];
    dateSelectionController.datePicker.maximumDate = maxDate;
    dateSelectionController.datePicker.datePickerMode = UIDatePickerModeDateAndTime;
    
    //Now just present the date selection controller using the standard iOS presentation method
    [self presentViewController:dateSelectionController animated:YES completion:nil];
}
- (IBAction)schedulePost:(id)sender {
    if([_txtTitle.text isEqualToString:@""]){
        [self simpleAlert:string_invalid_post_title withDelegate:self.view];
    }else if([albumFileArray count] == 0 && !videoUrl && !chooseImage){
        [self simpleAlert:string_invalid_post_media withDelegate:self.view];
        if(isVideo == YES){
            if(!videoUrl){
                [self simpleAlert:string_invalid_post_media withDelegate:self.view];
            }else{
                [self fileUpdated:isChooseCamera];
            }
        }else if(isVideo == NO){
            if(!chooseImage){
                [self simpleAlert:string_invalid_post_media withDelegate:self.view];
            }else{
                [self fileUpdated:isChooseCamera];
            }
        }else if([albumFileArray count] == 0){
            [self simpleAlert:string_invalid_post_media withDelegate:self.view];
        }
    }else{
        [self showScheduleTime];
    }
}

-(void)scheduleConfirmation{
    CustomYesNoAlert  *customAlert= [[[NSBundle mainBundle] loadNibNamed:@"CustomYesNoAlert"
                                                                   owner:self
                                                                 options:nil] objectAtIndex:0];
    customAlert.layer.masksToBounds = true;
    customAlert.clipsToBounds = true;
    customAlert.whiteBGLayer.layer.cornerRadius = 6.0f;
    customAlert.whiteBGLayer.layer.masksToBounds = YES;
    customAlert.whiteBGLayer.clipsToBounds = YES;
    customAlert.touchButton=self;
    NSString * scheduleMessage = [NSString stringWithFormat:@"Do you want to schedule the event on? %@ ?",[self.btnSchedulePost currentTitle]];
    customAlert.message.text = scheduleMessage;
    
    KLCPopupLayout layout = KLCPopupLayoutMake(KLCPopupHorizontalLayoutCenter,
                                               (KLCPopupVerticalLayout)KLCPopupVerticalLayoutCenter);
    
    _popup = [KLCPopup popupWithContentView:customAlert
                                   showType:KLCPopupShowTypeFadeIn
                                dismissType:KLCPopupDismissTypeFadeOut
                                   maskType:KLCPopupMaskTypeDimmed
                   dismissOnBackgroundTouch:YES
                      dismissOnContentTouch:NO];
    [_popup showWithLayout:layout];
}
-(void)viewWillAppear:(BOOL)animated{
    NSError *err;
//    self.locationmanager = [[CLLocationManager alloc]init];
//    [self.locationmanager setDesiredAccuracy:kCLLocationAccuracyBest];
//    self.locationmanager.distanceFilter = 500;
//    [self.locationmanager startUpdatingLocation];
//    [self.locationmanager requestWhenInUseAuthorization];
//    
//    geocoder = [[CLGeocoder alloc] init];

    //[self.navigationItem.leftBarButtonItem setEnabled:NO];
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"back_icon.png"] imageWithRenderingMode: UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(settingAction:)];
    self.navigationItem.leftBarButtonItem = barButtonItem;
    self.navigationItem.title = @"Create Live Event";
    self.navigationController.navigationBar.titleTextAttributes = @{
                                                                    NSFontAttributeName:[UIFont fontWithName:@"KaushanScript-Regular" size:18],
                                                                    NSForegroundColorAttributeName: [UIColor whiteColor]
                                                                    };

    if (![[NSFileManager defaultManager] createDirectoryAtPath:[NSTemporaryDirectory() stringByAppendingPathComponent:@"upload"]
                                   withIntermediateDirectories:YES
                                                    attributes:nil
                                                         error:&err]) {
        NSLog(@"reating 'upload' directory failed: [%@]", err);
    }
    self.serviceDelegate.delegate = self;
    self.appUtil.alrDelegate = self;
    [self.serviceDelegate resetRKObjectManager];
    [self configuration];
}
- (void)settingAction:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Configuration
-(void)configuration{
    _btnAddPhotos.layer.borderColor = [UIColor colorWithRed:255.0/255.0 green:203.0/255.0 blue:26.0/255.0 alpha:1.0].CGColor;
    _btnAddPhotos.layer.borderWidth = 2.0;
    
    _txtTitle.floatLabelActiveColor = [UIColor orangeColor];
    _txtAddDesc.floatLabelActiveColor = [UIColor orangeColor];
    _txtHost.floatLabelActiveColor = [UIColor orangeColor];
    
    [Lib bootomBorder:_txtTitle bottomColor:[UIColor lightGrayColor]];
    [Lib bootomBorder:_txtAddDesc bottomColor:[UIColor lightGrayColor]];
    [Lib bootomBorder:_txtHost bottomColor:[UIColor lightGrayColor]];
}

- (BOOL)textField:(UIFloatLabelTextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    // Prevent crashing undo bug – see note below.
    if(textField == _txtTitle){
        NSUInteger newLength = [_txtTitle.text length] + [string length] - range.length;
        return newLength <= 30;
    }else{
        return YES;
    }
}

#pragma mark - GMImagePickerControllerDelegate
- (void)assetsPickerController:(GMImagePickerController *)picker didFinishPickingAssets:(NSArray *)assetArray{
    //isChooseCamera = NO;
    setGMPicker = picker;
    //[picker.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    albumFileArray = (NSMutableArray *)assetArray;
    for (int i=0; i<assetArray.count; i++) {
        PHAsset *temAsset = (PHAsset *)[assetArray objectAtIndex:i];
        if (temAsset.mediaType == 2) {
            video = video + 1;
        }else{
            photo = photo + 1;
        }
    }
    
    if (assetArray.count == 0) {
        [photoFileArray removeAllObjects];
        [videoFileArray removeAllObjects];
    }else{
        for (int i = 0; i < assetArray.count; i++) {
            PHAsset *temAsset = (PHAsset *)[assetArray objectAtIndex:i];
            if (temAsset.mediaType == 2) {
                isChooseCamera = NO;
                [picker.presentingViewController dismissViewControllerAnimated:YES completion:nil];
            }else{
                isChooseCamera = YES;
                PHImageRequestOptions * imageRequestOptions = [[PHImageRequestOptions alloc] init];
                imageRequestOptions.synchronous = YES;
                imageRequestOptions.networkAccessAllowed = YES;
                imageRequestOptions.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat;
                imageRequestOptions.resizeMode = PHImageRequestOptionsResizeModeFast;
                
                if(temAsset.pixelHeight > 1000 && temAsset.pixelWidth > 1000){
                    [[PHImageManager defaultManager] requestImageForAsset:temAsset targetSize:CGSizeMake(700, 700)  contentMode:PHImageContentModeAspectFit options:imageRequestOptions resultHandler:^(UIImage * _Nullable result, NSDictionary * _Nullable info) {
                        chooseImage = result;
                    }];
                }else{
                    [[PHImageManager defaultManager] requestImageForAsset:temAsset targetSize:PHImageManagerMaximumSize contentMode:PHImageContentModeAspectFit options:imageRequestOptions resultHandler:^(UIImage * _Nullable result, NSDictionary * _Nullable info) {
                        chooseImage = result;
                    }];
                }
                UIAlertView *logoutAlert = [[UIAlertView alloc] initWithTitle:nil message: [NSString stringWithFormat: @"You want to crop this image ?"] delegate: self cancelButtonTitle: @"YES"  otherButtonTitles:@"NO",nil];
                [logoutAlert setTag:123];
                [logoutAlert setDelegate:self];
                [logoutAlert show];

            }
        }

        //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fileUpdated:) name:@"firstImageUpload" object:nil];
    }
    
    if (photo > 0 && video > 0) {
        if (video == 1 && photo ==  1) {
            _gallerInfoLbl.text = [NSString stringWithFormat:@"%d Photo and %d Video added",photo, video];
        }else if (video > 1 && photo == 1){
            _gallerInfoLbl.text = [NSString stringWithFormat:@"%d Photo and %d Videos added",photo, video];
        }else if (video == 1 && photo > 1){
            _gallerInfoLbl.text = [NSString stringWithFormat:@"%d Photo and %d Video added",photo, video];
        }else{
            _gallerInfoLbl.text = [NSString stringWithFormat:@"%d Photo and %d Videos added",photo, video];
        }
    }else if (photo > 0){
        if (video == 1) {
            _gallerInfoLbl.text = [NSString stringWithFormat:@"%d Photo added",photo];
        }else{
            _gallerInfoLbl.text = [NSString stringWithFormat:@"%d Photo added",photo];
        }
    }else if (video > 0){
        if (video == 1) {
            _gallerInfoLbl.text = [NSString stringWithFormat:@"%d Video added", video];
        }else{
            _gallerInfoLbl.text = [NSString stringWithFormat:@"%d Videos added", video];
        }
    }
}

- (IBAction)postNow:(id)sender {
    if([_txtTitle.text isEqualToString:@""]){
        [self simpleAlert:string_invalid_post_title withDelegate:self.view];
    }else if([albumFileArray count] == 0 && !chooseImage){
            [self simpleAlert:string_invalid_post_media withDelegate:self.view];
                if(!chooseImage){
                    [self simpleAlert:string_invalid_post_media withDelegate:self.view];
                }else{
                    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];

                    [self.appUtil showActivityIndicator:self.view];
                    [self fileUpdated:isChooseCamera];
                }
        }else{
                [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
                [self.appUtil showActivityIndicator:self.view];
                [self fileUpdated:isChooseCamera];
        }
}

-(void) didTouchbtnYes: (id) sender{
    [_btnPostNow setHidden:YES];
    isSchedule = YES;
    [_popup dismissPresentingPopup];
    [self.appUtil showActivityIndicator:self.view];
    [self fileUpdated:isChooseCamera];
}
-(void) didTouchbtnNo: (id) sender{
    [_btnPostNow setHidden:NO];
    isSchedule = NO;
    [_popup dismissPresentingPopup];
}
-(void)livePageAction{
    [[NSNotificationCenter defaultCenter] removeObserver:self];

    if(isSchedule){
        LiveListViewController *logObj =[[LiveListViewController alloc] initWithNibName:@"LiveListViewController" bundle:nil];
        logObj.mainViewController = self.navigationController;
        [self showViewController:logObj sender:nil];
 
    }else{
        [[NSNotificationCenter defaultCenter] removeObserver:self];
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:string_main bundle:nil];
        VideoPlayerViewController * postObj =[storyboard instantiateViewControllerWithIdentifier:@"VideoPlayerViewController"];
        postObj.mainViewController = self.mainViewController;
        postObj.livePortNumber = [NSString stringWithFormat:@"%@",[returnData objectForKey:@"livePort"]];
        postObj.liveStreamName = [NSString stringWithFormat:@"%@",[returnData objectForKey:@"liveStreamName"]];
        postObj.liveHostAddress = [NSString stringWithFormat:@"%@",[returnData objectForKey:@"liveHost"]];
        postObj.liveApplicationName = [NSString stringWithFormat:@"%@",[returnData objectForKey:@"liveApplication"]];
        postObj.liveStreamId = [NSString stringWithFormat:@"%@",[returnData objectForKey:@"liveId"]];
        
        //    postObj.liveUserName = [NSString stringWithFormat:@"%@",[returnData objectForKey:@"liveUsername"]];
        //    postObj.livePassword = [NSString stringWithFormat:@"%@",[returnData objectForKey:@"livePassword"]];
        
        postObj.streamFlag = @"Yes";
        [self showViewController:postObj sender:nil];
    }
}
-(void)createLivePostInfo{
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center removeObserver:self name:@"PostInfo" object:nil];
    [self.appUtil hideActivityIndicator:self.view];
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    ResponseHandler callback = ^(BOOL wasSuccessful, RKMappingResult *mappingResult,RKObjectRequestOperation *operation) {
        DLOG(@"callback reponse %@",operation.HTTPRequestOperation.responseString);
        if(operation.HTTPRequestOperation.response.statusCode == 200){
            NSString* userString=operation.HTTPRequestOperation.responseString;
            returnData = [NSJSONSerialization JSONObjectWithData:[userString dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];

            if ([[returnData allKeys] containsObject:@"_id"] == YES) {
                [self.navigationItem.leftBarButtonItem setEnabled:YES];
                self.imageId = [returnData objectForKey:@"_id"];
                [self CreateLivePost];
            }else{
                [self.appUtil singleAlertControllerTitle:@"" and:@"" delegate:NO withBtnName:string_ok];
            }
        }
    };
    NSDictionary *params;
   // Get the current country code.
//    NSLocale *currentLocale = [NSLocale currentLocale];
//    NSString *countryCode = [currentLocale objectForKey:NSLocaleCountryCode];
    self.user = [[User alloc] initWithString:[Lib reteriveData:Access_Token] error:nil];
   
    NSMutableArray *hostedArray = [[NSMutableArray alloc] init];
    [hostedArray addObject:self.user.access_token.userInfo._id];
    //@"broadcast_location":countryCode,
    if(isSchedule){
        params = @{@"name": _txtTitle.text,@"postDescription":_txtAddDesc.text,@"tag":_txtHost.text,@"hostedUsers":hostedArray,@"publishedTime":scheduleTime};
    }else{
        params = @{@"name": _txtTitle.text,@"postDescription":_txtAddDesc.text,@"tag":_txtHost.text,@"hostedUsers":hostedArray};
    }
    [self.serviceDelegate executePostURL:LIVE_POST_URL param:params responseMapping:self.serviceDelegate.dummyMapping keyPath:nil callBack:callback onView:self.view];
}

- (void)fileUpdated :(BOOL)isFlag {
    if(isFlag == YES){
        [self uploadPostImg:chooseImage fromView:self.view isVideo:isVideo videoUrl:videoUrl];
    }else{
        [self uploadRequest:albumFileArray];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Button Actions
- (IBAction)backBtnAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    isChooseCamera = YES;
    _gallerInfoLbl.text = [NSString stringWithFormat:@"1 Photo added"];
    isVideo = NO;
    chooseImage = (UIImage *) [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    setPicker = picker;
    UIAlertView *logoutAlert = [[UIAlertView alloc] initWithTitle:nil message: [NSString stringWithFormat: @"You want to crop this image ?"] delegate: self cancelButtonTitle: @"YES"  otherButtonTitles:@"NO",nil];
    [logoutAlert setTag:123456];
    [logoutAlert setDelegate:self];
    [logoutAlert show];

    //[picker dismissViewControllerAnimated:YES completion:NULL];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if ([alertView tag]==123456) {
        if(buttonIndex==0){
            PhotoTweaksViewController *photoTweaksViewController = [[PhotoTweaksViewController alloc] initWithImage:chooseImage];
            photoTweaksViewController.delegate = self;
            photoTweaksViewController.pageFlag = @"Post";
            photoTweaksViewController.cameraFlag = @"Yes";
            photoTweaksViewController.autoSaveToLibray = YES;
            photoTweaksViewController.maxRotationAngle = M_PI_4;
            [setPicker pushViewController:photoTweaksViewController animated:YES];
        }else{
            [setPicker dismissViewControllerAnimated:YES completion:NULL];
        }
    }else if([alertView tag]==123){
        if(buttonIndex==0){
            PhotoTweaksViewController *photoTweaksViewController = [[PhotoTweaksViewController alloc] initWithImage:chooseImage];
            photoTweaksViewController.delegate = self;
            photoTweaksViewController.pageFlag = @"Post";
            photoTweaksViewController.autoSaveToLibray = YES;
            photoTweaksViewController.maxRotationAngle = M_PI_4;
            photoTweaksViewController.mainViewController = _mainViewController;
            [setGMPicker.presentingViewController dismissViewControllerAnimated:NO completion:nil];
            //[self.navigationController pushViewController:photoTweaksViewController animated:YES];
            [self.navigationController presentViewController:photoTweaksViewController animated:YES completion:NULL];

        }else{
            [setGMPicker.presentingViewController dismissViewControllerAnimated:YES completion:nil];
        }
    }
}

- (void)photoTweaksController:(PhotoTweaksViewController *)controller didFinishWithCroppedImage:(UIImage *)croppedImage camera:(NSString *)cameraValue{
    if([cameraValue isEqualToString:@"Yes"]){
        chooseImage = croppedImage;
        [controller dismissViewControllerAnimated:YES completion:NULL];
    }else{
        [albumFileArray removeAllObjects];
        chooseImage = croppedImage;
        // [self.navigationController popViewControllerAnimated:YES];
        [self.navigationController dismissViewControllerAnimated:YES completion:NULL];
    }
}

- (void)photoTweaksControllerDidCancel:(PhotoTweaksViewController *)controller camera:(NSString *)cameraValue{
    if([cameraValue isEqualToString:@"Yes"]){
        [setPicker dismissViewControllerAnimated:YES completion:NULL];
    }else{
        //[self.navigationController popViewControllerAnimated:YES];
        [self.navigationController dismissViewControllerAnimated:YES completion:NULL];
    }
}

- (BOOL)assetsPickerController:(GMImagePickerController *)picker shouldSelectAsset:(PHAsset *)asset{
    if([picker.selectedAssets count] < 1)
        return YES;
    else{
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Maximum Selected"
                                     message:@"You can only upload 1 media at once."
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"Ok"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        //Handle your yes please button action here
                                    }];
        
        [alert addAction:yesButton];
        //[self showViewController:alert sender:nil];
        [self presentViewController:alert animated:YES completion:nil];
        return NO;
    }
}

- (IBAction)addPhotos:(id)sender {
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Load image from: " message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    
    //Library button to add to the action sheet
    UIAlertAction *libraryAction = [UIAlertAction actionWithTitle:@"Library" style:UIAlertActionStyleDefault handler: ^(UIAlertAction *action){
        if ([PHPhotoLibrary authorizationStatus] == PHAuthorizationStatusAuthorized) {
            
            photo = 0;
            video = 0;
            GMImagePickerController *picker = [[GMImagePickerController alloc] init];
            [picker.navigationController.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
            picker.mediaTypes = @[@(PHAssetMediaTypeImage)];
            picker.title = @"SBN";
            picker.delegate = self;
            picker.colsInLandscape = 5;
            picker.colsInPortrait = 4;
           // picker.allowsMultipleSelection = NO;
            [self presentViewController:picker animated:NO completion:nil];
        }else{
            [self photoPermission:self];
        }
    }];
    
    //Camera button to add to the action sheet
    UIAlertAction *cameraAction = [UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler: ^(UIAlertAction *action){
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        picker.mediaTypes = [NSArray arrayWithObjects:(NSString *)kUTTypeImage, nil];
        picker.cameraCaptureMode = UIImagePickerControllerCameraCaptureModePhoto;
        picker.delegate = self;
        picker.modalPresentationStyle = UIModalPresentationPopover;
        picker.videoQuality = UIImagePickerControllerQualityTypeHigh;
        
        UIPopoverPresentationController *popPC = picker.popoverPresentationController;
        popPC.permittedArrowDirections = UIPopoverArrowDirectionAny;
          [self showViewController:picker sender:sender];
    }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler: ^(UIAlertAction *action){
    }];
    
    [actionSheet addAction:libraryAction];
    [actionSheet addAction:cameraAction];
    [actionSheet addAction:cancelAction];
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}

-(void)checkCameraService:(BOOL)shouldOnlyCheckStatus{
    
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if(authStatus == AVAuthorizationStatusAuthorized) {
        // do your logic
        isCameraAccess = YES;
    } else if(authStatus == AVAuthorizationStatusDenied){
        // denied
        if(!shouldOnlyCheckStatus)
            [self showAlert];
    } else if(authStatus == AVAuthorizationStatusRestricted){
        // restricted, normally won't happen
        if(!shouldOnlyCheckStatus)
            [self showAlert];
    } else if(authStatus == AVAuthorizationStatusNotDetermined){
        //if(!shouldOnlyCheckStatus)
        [self showAlert];
    } else {
        // impossible, unknown authorization status
    }
}

-(void)showAlert{
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Permission Denied"
                                 message:@"In order to take a picture, please go to Settings and turn on Camera Access for this app."
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"Ok"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    //Handle your yes please button action here
                                }];
    
    [alert addAction:yesButton];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)singleAlertBtnClicked{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - #pragma mark - TEXT FIELD DELEGATE
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    textField.placeholder = [textField.placeholder uppercaseString];
}

- (IBAction)textValueDidChange:(UITextField *)sender {
    if (sender.text.length > 0) {
        [Lib bootomBorder:(UIFloatLabelTextField *)sender bottomColor:self.primaryColor];
    }else{
        [Lib bootomBorder:(UIFloatLabelTextField *)sender bottomColor:[UIColor lightGrayColor]];
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    if (textField.text.length > 0) {
        [Lib bootomBorder:(UIFloatLabelTextField *)textField bottomColor:self.primaryColor];
        textField.placeholder = [textField.placeholder uppercaseString];
    }else{
        [Lib bootomBorder:(UIFloatLabelTextField *)textField bottomColor:[UIColor lightGrayColor]];
        textField.placeholder = [textField.placeholder capitalizedString];
    }
}

-(BOOL)textFieldShouldReturn:(UITextField*)textField{
    UIResponder* nextResponder = [textField.superview viewWithTag:textField.tag + 1];
    if (textField.tag == 100) {
        return [nextResponder becomeFirstResponder];
    }else if (textField.tag == 101){
        return [nextResponder becomeFirstResponder];
    }else{
        return [textField resignFirstResponder];
    }
}

@end
