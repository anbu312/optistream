//  EnlargePicViewController.m
//  Hype
//
//  Created by Sathish on 22/03/16.
//  Copyright © 2016 Optisol Business Solution 
//

#import "EnlargeLiveViewController.h"
#import "ProfileViewController.h"
#import "UIImageView+WebCache.h"
#import "LikeManager.h"
#import "Post.h"
#import "DetailedPostViewController.h"
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <Social/Social.h>
#import "FeedPost.h"
#import "CustomYesNoAlert.h"
#import "KLCPopup.h"
#import "HomeViewController.h"
#import "LiveListViewController.h"
#import "ChatViewBottomContainer.h"
#import "ChatViewController.h"
#import "LiveViewersProfileViewController.h"

NSInteger const CHATVIEW_BOTTOM_CONTAINER = 318;

@interface EnlargeLiveViewController ()<JWPlayerDelegate,UIPageViewControllerDelegate,UIPageViewControllerDataSource,UIGestureRecognizerDelegate,ChatViewControllerDelegate>{
    
    NSMutableArray * dict;
    
    IBOutlet ChatViewBottomContainer *chatViewBottomContainer;
    NSString *tempTwillioToken;
    
    int currentDeviceWidth;
    int currentDeviceHeight;
}

@property Post *post;
@property FeedPost *feedPostList;

@property JWPlayerController *playerView;
@property (strong, nonatomic) UIView *videoPlayer;
@property (strong, nonatomic) UIView *videoLayer;

@property (strong, nonatomic) NSTimer *playbackTimeCheckerTimer;
@property (assign, nonatomic) CGFloat videoPlaybackPosition;

@property (strong, nonatomic) AVAsset *asset;

@property (assign, nonatomic) CGFloat startTime;
@property (assign, nonatomic) CGFloat stopTime;
@property (nonatomic) IBOutlet UIButton *playButton;

@property KLCPopup* popup ;

@property (nonatomic,strong) ChatViewController *chatViewController;
@property (nonatomic,strong) LiveViewersProfileViewController *liveViewersProfileVC;
@property (nonatomic,strong) UIPageViewController *PageViewController;
@property (nonatomic,strong) NSArray *arrPageTitles;
@property (nonatomic,strong)  LiveListViewController*listVal;

@end

@implementation EnlargeLiveViewController

@synthesize currentPostId,playerView,pageFlag;

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"NOTIFICATION_POSTID"];
    dict = [[NSMutableArray alloc] init];
    
    if([_isLiveFlag boolValue] == 1){
        [self watchUpdateData];
    }
    
    currentDeviceWidth = [Lib windowWidth];
    currentDeviceHeight = [Lib windowHeight];
    
    [self videoPlay:_liveUrl];
    [self chatUIConfiguratoin];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.user = [[User alloc] initWithString:[Lib reteriveData:Access_Token] error:nil];
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    self.serviceDelegate.delegate = self;
    self.appUtil.alrDelegate = self;
    [self.serviceDelegate resetRKObjectManager];
    [AppDelegate restrictRotation:NO];

    //[self addSubUIComponents:NO];
//    if([[UIDevice currentDevice] orientation] == UIDeviceOrientationPortrait){
//        [self chatAction:NO];
//    }else{
//        [self chatAction:YES];
//    }

    //[self chatAction:NO];
}

-(void)viewWillDisappear:(BOOL)animated{
    if([_isLiveFlag boolValue] == 1){
        [self removeWatchList];
    }
    [self.playerView stop];
    [self.playerView.view removeFromSuperview];
    [AppDelegate restrictRotation:YES];
}

- (void)viewDidDisappear:(BOOL)animated{
    [AppDelegate restrictRotation:YES];
}

- (IBAction)goBack:(UIButton *)sender {
    [self.playerView stop];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)closeButtonTouched:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)chatUIConfiguratoin{
    _arrPageTitles = @[@"IPChatView"];
    
    // Rerfame Chat Bottom Container
    chatViewBottomContainer.backgroundColor = [UIColor clearColor];
    
    // Create a new inner view controllers
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:string_main bundle:nil];
    _chatViewController = [storyboard instantiateViewControllerWithIdentifier:@"ChatViewControllerID"];
    //_liveViewersProfileVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LiveViewersProfileViewControllerID"];
    
    // Set delegate
    _chatViewController.chatViewDelegate = self;
    
    // Re-framing both inner view controllers
    [_chatViewController setFrame:chatViewBottomContainer];
    //_liveViewersProfileVC.view.frame = _chatViewController.view.frame;
    
    // Set page index
    _chatViewController.pageIndex = 0;
    //_liveViewersProfileVC.pageIndex = 1;
    
    // Create page view controller
    self.PageViewController = [storyboard instantiateViewControllerWithIdentifier:@"ChatPagerID"];
    self.PageViewController.dataSource = self;
    NSArray *viewControllers = @[_chatViewController];
    [self.PageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    // Change the size of page view controller
    self.PageViewController.view.frame = CGRectMake(0, 0, chatViewBottomContainer.frame.size.width, chatViewBottomContainer.frame.size.height);
    
    // Add it into bottom container
     [chatViewBottomContainer addSubview:self.PageViewController.view];
    //[chatViewBottomContainer bringSubviewToFront:self.PageViewController.view];
    [self.PageViewController didMoveToParentViewController:self];
    
    [self.playerView.view addSubview:chatViewBottomContainer];
    [self getTempChatToken:_liveID device:[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"deviceToken"]]];
}

-(void)getTempChatToken:(NSString *)liveId device:(NSString *)deviceId{
    [[[RKObjectManager sharedManager] HTTPClient] setDefaultHeader:@"Content-Type" value:@"application/json"];
    ResponseHandler callback = ^(BOOL wasSuccessful, RKMappingResult *mappingResult,RKObjectRequestOperation *operation) {
        DLOG(@"callback reponse %@",operation.HTTPRequestOperation.responseString);
        NSDictionary *tempDic = [NSJSONSerialization JSONObjectWithData:[operation.HTTPRequestOperation.responseString dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
        tempTwillioToken = [tempDic valueForKey:@"token"];
        [self connectTwilioChatClient];
        
    };
    [self.serviceDelegate executeGetURLNotLoading:[NSString stringWithFormat:LIVE_CHAT_TOKEN_URL,liveId,deviceId]  param:nil responseMapping:self.serviceDelegate.dummyMapping keyPath:nil callBack:callback onView:self.view];
}

-(void)stateCallProgress{
    ResponseHandler callback = ^(BOOL wasSuccessful, RKMappingResult *mappingResult,RKObjectRequestOperation *operation) {
        DLOG(@"callback reponse %@",operation.HTTPRequestOperation.responseString);
        if(operation.HTTPRequestOperation.response.statusCode == 200){
           dict  = [NSJSONSerialization JSONObjectWithData:[operation.HTTPRequestOperation.responseString dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
            NSDictionary *userDic = [dict valueForKey:@"live_stream"];
            NSString *currState = [NSString stringWithFormat:@"%@",[userDic valueForKey:@"state"]];
            if([currState isEqualToString:@"stopped"]){
                [self.appUtil singleAlertControllerTitle:@"This Live video has ended." and:@"It will be available to watch on SBN's live events shortly." delegate:YES withBtnName:string_ok];
            }
        }
    };
    NSString *tempUrl= [NSString stringWithFormat:@"%@%@",STREAM_STATE,_liveID];
    [self.serviceDelegate executeGetURLNotLoading:tempUrl param:nil responseMapping:self.serviceDelegate.dummyMapping keyPath:nil callBack:callback onView:self.view];
}

-(void)singleAlertBtnClicked{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    LiveListViewController *logObj =[[LiveListViewController alloc] initWithNibName:@"LiveListViewController" bundle:nil];
    logObj.mainViewController = self.navigationController;
    [self showViewController:logObj sender:nil];
}

-(void)removeWatchList{
    ResponseHandler callback = ^(BOOL wasSuccessful, RKMappingResult *mappingResult,RKObjectRequestOperation *operation) {
        DLOG(@"callback reponse %@",operation.HTTPRequestOperation.responseString);
        if(operation.HTTPRequestOperation.response.statusCode == 200){
            
        }
    };
    NSString *tempUrl= [NSString stringWithFormat:@"%@%@",WATCH_LIST,_liveID];
    [self.serviceDelegate executeDeleteURL:tempUrl param:nil responseMapping:self.serviceDelegate.dummyMapping keyPath:nil callBack:callback onView:self.view];
}

-(void)watchUpdateData{
    ResponseHandler callback = ^(BOOL wasSuccessful, RKMappingResult *mappingResult,RKObjectRequestOperation *operation) {
        DLOG(@"callback reponse %@",operation.HTTPRequestOperation.responseString);
        if(operation.HTTPRequestOperation.response.statusCode == 200){
            
        }
    };
    NSString *tempUrl= [NSString stringWithFormat:@"%@%@",WATCH_LIST,_liveID];
    [self.serviceDelegate executePostLinkedURL:tempUrl param:nil responseMapping:self.serviceDelegate.dummyMapping keyPath:nil callBack:callback onView:self.view];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)videoPlay:(NSString *) fileUrl{
 
    JWConfig *config = [JWConfig new];
    config.file = fileUrl;
    config.controls = NO;
    config.repeat = NO;   //default
    config.autostart = YES;
    config.stretch = JWStretchUniform;
    
    playerView = [[JWPlayerController alloc] initWithConfig:config];
    playerView.delegate = self;
    playerView.view.frame = CGRectMake(self.playerContainer.frame.origin.x, self.playerContainer.frame.origin.y, self.playerContainer.frame.size.width, self.playerContainer.frame.size.height);
    
    self.playerView.forceFullScreenOnLandscape = YES;
    self.playerView.forceLandscapeOnFullScreen = YES;
    [self.view addSubview:playerView.view];
    [self.playerView.view addSubview:_chatBtn];
    [self.playerView.view addSubview:_liveBtn];
    [self.playerView.view addSubview:_backBtn];
}

-(void)onPlay{
    [[AVAudioSession sharedInstance]
     setCategory: AVAudioSessionCategoryPlayback
     error: nil];
}

-(void)onBuffer{
    if([_isLiveFlag boolValue] == 1)
        [self stateCallProgress];
}
-(void)chatAction:(BOOL)isLandscape{
    _backBtn.frame = CGRectMake(0, 10, _backBtn.frame.size.width, _backBtn.frame.size.height);
    
    _liveBtn.frame = CGRectMake([[UIDevice currentDevice] orientation] == UIDeviceOrientationPortrait?currentDeviceWidth- 70:currentDeviceHeight - 70, 20, _liveBtn.frame.size.width, _liveBtn.frame.size.height);
    
    _chatBtn.frame = CGRectMake(10, [[UIDevice currentDevice] orientation] == UIDeviceOrientationPortrait?currentDeviceHeight -46:currentDeviceWidth -46, _chatBtn.frame.size.width, _chatBtn.frame.size.height);
    
    chatViewBottomContainer.frame = CGRectMake(0, [[UIDevice currentDevice] orientation] == UIDeviceOrientationPortrait?currentDeviceHeight - (currentDeviceHeight/2) -44:currentDeviceWidth - (currentDeviceWidth/2) -44,
                                               [[UIDevice currentDevice] orientation] == UIDeviceOrientationPortrait?currentDeviceWidth:currentDeviceHeight, [[UIDevice currentDevice] orientation] == UIDeviceOrientationPortrait?currentDeviceHeight-(currentDeviceHeight/2):currentDeviceWidth - (currentDeviceWidth/2));
}
- (void)addSubUIComponents:(BOOL)isLandscape{
    
    playerView.config.stretch = JWStretchUniform;
    playerView.view.frame = CGRectMake(self.playerContainer.frame.origin.x, self.playerContainer.frame.origin.y, self.playerContainer.frame.size.width, self.playerContainer.frame.size.height);
    
    _backBtn.frame = CGRectMake(0, 10, _backBtn.frame.size.width, _backBtn.frame.size.height);
    
    _liveBtn.frame = CGRectMake([[UIDevice currentDevice] orientation] == UIDeviceOrientationPortrait?currentDeviceWidth- 70:currentDeviceHeight - 70, 20, _liveBtn.frame.size.width, _liveBtn.frame.size.height);
    
    _chatBtn.frame = CGRectMake(10, [[UIDevice currentDevice] orientation] == UIDeviceOrientationPortrait?currentDeviceHeight -46:currentDeviceWidth -46, _chatBtn.frame.size.width, _chatBtn.frame.size.height);
    
    chatViewBottomContainer.frame = CGRectMake(0, [[UIDevice currentDevice] orientation] == UIDeviceOrientationPortrait?currentDeviceHeight - (currentDeviceHeight/2) -44:currentDeviceWidth - (currentDeviceWidth/2) -44,
                                               [[UIDevice currentDevice] orientation] == UIDeviceOrientationPortrait?currentDeviceWidth:currentDeviceHeight, [[UIDevice currentDevice] orientation] == UIDeviceOrientationPortrait?currentDeviceHeight-(currentDeviceHeight/2):currentDeviceWidth - (currentDeviceWidth/2));
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator{
    [[UIDevice currentDevice] orientation] == UIDeviceOrientationPortrait?[self addSubUIComponents:NO]:[self addSubUIComponents:YES];
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
}

#pragma mark - Live Chat Implementation
//################################# LIVE CHAT IMPLEMENTATION  #######################//

- (void)connectTwilioChatClient{
    NSString *tempUserId = [NSString stringWithFormat:@"%@",self.user.access_token.userInfo._id];
    [_chatViewController connectWithTwilioClient:tempUserId token:tempTwillioToken channel:_liveID];
    //[self enableUserChat];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index;
    
    if ([viewController isKindOfClass:ChatViewController.class]) {
        index = ((ChatViewController*) viewController).pageIndex;
    }else{
        index = ((LiveViewersProfileViewController*) viewController).pageIndex;
    }
    
    if ((index == 0) || (index == NSNotFound))
    {
        return nil;
    }
    
    index--;
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index;
    
    if ([viewController isKindOfClass:ChatViewController.class]) {
        index = ((ChatViewController*) viewController).pageIndex;
    }else{
        index = ((LiveViewersProfileViewController*) viewController).pageIndex;
    }
    
    if (index == NSNotFound)
    {
        return nil;
    }
    
    index++;
    if (index == [self.arrPageTitles count])
    {
        return nil;
    }
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)viewControllerAtIndex:(NSUInteger)index
{
    if (([self.arrPageTitles count] == 0) || (index >= [self.arrPageTitles count])) {
        return nil;
    }
    NSLog(@"index.... %ld",index);
    index == 0?(_chatViewController.pageIndex = index):(_liveViewersProfileVC.pageIndex = index);
    return index==0?_chatViewController:_liveViewersProfileVC;
}

-(IBAction)beginChat:(id)sender{
    
    [self.view bringSubviewToFront:chatViewBottomContainer];
    NSArray *viewControllers = @[_chatViewController];
    [self.PageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionReverse animated:YES completion:nil];
    [self performSelector:@selector(bringInputBar) withObject:nil afterDelay:0.4];
}

-(IBAction)liveUsers:(id)sender{
    NSArray *viewControllers = @[_liveViewersProfileVC];
    [self.PageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
    [self performSelector:@selector(loadViewers) withObject:nil afterDelay:1];
}

- (void)loadViewers{
    [_liveViewersProfileVC refereshUserList];
}

-(void)bringInputBar{
    [_chatViewController showKeyBoardWithInputBar];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gesture shouldReceiveTouch:(UITouch *)touch {
    if (![touch.view.superview isKindOfClass:[UITableViewCell class]]){
        return YES;
    }
    return NO;
}

// Hide bottom container - By tap the view, can be used to hide/show all components when we are in live streaming
- (void)hideOrShowChatViewBottomContainer{
    chatViewBottomContainer.hidden = !chatViewBottomContainer.hidden;
}

-(void)enableUserChat{
    self.chatBtn.userInteractionEnabled = YES;
    [self.chatBtn setImage:[UIImage imageNamed:@"chat"] forState:UIControlStateNormal];
}

@end
